# Camagru

# Table of content
1. [Installation](#installation)
   1. [I don't have any Docker Machine created](#i-dont-have-any-docker-machine-created)
   2. [I already created a Docker Machine](#i-already-created-a-docker-machine)
2. [First Run](#first-run)
3. [Containers updates and custom configurations](#containers-updates-and-custom-configurations)
   1. [Reinstall containers](#reinstall-containers-or-database)
   2. [Edit Apache2 conf file](#edit-apache2-conf-file)
   3. [Add custom .conf file](#add-custom-conf-file)
4. [Autor](#author)

## Installation
### I don't have any Docker Machine created
If you do not have any Docker machine created:
```bash
./reinstall -b
```
This script will automatically detect if the Docker Machine exists (by default named: `Camagru`) and runs it.
<br>This script has been tested on macOS X, it may broke on other operating system.

### I already created a Docker Machine
If you already created a Docker Machine, check first that your Docker Machine name is
<br>similar to the DOCKER_MACHINE_NAME at the top of `reinstall` file
<br>otherwise please update `reinstall` file (if you don't, the script will automatically create
<br>new Docker Machine after DOCKER_MACHINE_NAME)
<br>Then you are good to run:
```bash
./reinstall
```
or if you want to kill the previous docker instance:
```bash
./reinstall -bk
```

## First Run
Once your Docker Machine is started and its environment is loaded into your `bash`,
<br>you can run:
```bash
$> ./reinstall -bG
```
The script will do following:
- Generate SSL files for HTTPS capability (file are saved under `config/cert` folder)
- Creating the `.env` file for the `docker-compose.yml` file
- Deleting existing containers
- Copy the config folder into the web working directory (`www`)
- Stop all related `docker-compose` processes
- Delete persistent volumes
- run `docker-compose up -b`

## Containers updates and custom configurations
### Reinstall containers or database
You can update your starting database by editing the `config/sql/camagru.sql` file

### Edit `Apache2` conf file
You can edit the `.conf` file for the `Apache` server. File it located at `config/conf/camagru.conf`
### Add custom `.conf` file
You can add your own `.conf` file:
- Copy it under `config/conf/my_conf_file.conf`
- Edit `www/Dockerfile COPY` command:
```diff
diff a/www/Dockerfile b/www/Dockerfile
--- a/www/Dockerfile
+++ b/www/Dockerfile
@@ -4,9 +4,9 @@
RUN apt-get update && apt-get install -y zlib1-dev
RUN docker-php-ext-install pdo pdo_mysql
COPY config/cert/server.crt /etc/apache2/ssl/server.crt
COPY config/cert/server.key /etc/apache2/ssl/server.key
- COPY config/conf/camagru.conf /etc/apache2/sites-available/camagru.conf
+ COPY config/conf/my_conf_file.conf /etc/apache2/sites-available/my_conf_file.conf
RUN a2enmod rewrite
- RUN a2ensite camagru.conf
+ RUN a2ensite my_conf_file.conf
RUN a2enmod ssl
RUN service apache2 restart
```

# Author
Edouard Jubert
<br>edjubert@student.42.fr

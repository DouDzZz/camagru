<?php
require_once($_SERVER['DOCUMENT_ROOT']."/controller/cameraController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/loginController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/profileController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/forgotPasswordController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/uploadPhoto.php");

if (session_status() != PHP_SESSION_ACTIVE)
	session_start();
$_SESSION['dark'] = "false";

/* print_r($_POST); */
/* print_r($_SESSION); */

try {
	if (isset($_POST['sendmail'])) {
		sendMail();
	}
	elseif (isset($_POST['resend']) && isset($_POST['email'])) {
		resendMail($_POST['resend']);
	}
	elseif (isset($_GET['activate'])) {
		activateAccount();
	}
	elseif (isset($_GET['retrieve'])) {
		getNewPasswordPage();
	}
	elseif (isset($_POST['changePassword'])) {
		setNewPassword($_POST['email'], $_POST['password'], $_POST['confirm']);
	}
	elseif (isset($_POST['footer']) && $_POST['footer'] === "about") {
		getAboutPage();
	}
	elseif (isset($_POST['forgotPassword'])) {
		getForgotPassword();
	}
	elseif (isConnected() == 1 && isUserActive() == 1) {
		$_SESSION['dark'] = userDarkMode($_SESSION['connected']);

		if (isset($_POST['header']) && $_POST['header'] === "logout") {
			logout();
		}
		elseif (isset($_POST['header']) && $_POST['header'] === "camera") {
			getCameraPage();
		}
		elseif (isset($_POST['header']) && $_POST['header'] === "home") {
			getHomePage();
		}
		elseif (isset($_POST['header']) && $_POST['header'] === "profile") {
			getProfilePage();
		}
		elseif (isset($_POST['img']) && $_POST['img'] === "inputfile") {
			postProfilePicture('inputfile');
		}
		else {
			getHomePage();
		}
	}
	else {
		if ((isset($_POST['signup']) && $_POST['signup'] === "signup")
			|| (isset($_POST['changeForm']) && $_POST['changeForm'] === "signup")) {
			getSignupPage("normal");
		}
		elseif ((isset($_POST['login']))
			|| (isset($_POST['changeForm']) && $_POST['changeForm'] === "login")) {
			getLoginPage();
		}
		elseif (isset($_POST['header']) && $_POST['header'] === "camera") {
			getLoginPage();
		}
		else {
			getHomePage();
		}
	}
}
catch (Exception $e) {
	require("view/errors/sorry.php");
}

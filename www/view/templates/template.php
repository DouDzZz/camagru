<?php
if (session_status() != PHP_SESSION_ACTIVE)
	session_start();
$dark = $_SESSION['dark'];
if ($dark === "root" && file_exists("public/css/variables.root.css"))
	$variables = "public/css/variables.root.css";
elseif ($dark === "true" && file_exists("public/css/variables.dark.css"))
	$variables = "public/css/variables.dark.css";
elseif ($dark === "false" && file_exists("public/css/variables.light.css"))
	$variables = "public/css/variables.light.css";
else
	$variables = "";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $title ?></title>
	<style>body {margin: 0;}</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<link rel="stylesheet" href="<?= $variables ?>">
	<link rel="stylesheet" href="<?= file_exists("public/css/variables.structure.css") ? 'public/css/variables.structure.css' : '' ?>">
	<link rel="stylesheet" href="<?= file_exists("public/css/camagru.css") ? 'public/css/camagru.css' : '' ?>">
	<link rel="stylesheet" href="<?= file_exists($style) ? $style : '' ?>">
</head>
<body>
	<?php file_exists($header) ? require_once($header) : require_once($_SERVER['DOCUMENT_ROOT']."/public/html/defaultHeader.php") ?>
	<div class="content"><?= $content ?></div>
	<?php file_exists($footer) ? require_once($footer) : require_once($_SERVER['DOCUMENT_ROOT']."/public/html/defaultFooter.html") ?>
	<?php if (file_exists($ajaxFunctions)) { ?>
		<script type="text/javascript" src="<?= $ajaxFunctions ?>"></script>
	<?php } ?>
	<?php if (file_exists($jsFunctions)) { ?>
		<script type="text/javascript" src="<?= $jsFunctions ?>"></script>
	<?php } ?>
	<?php if (file_exists($js)) { ?>
		<script type="text/javascript" src="<?= $js ?>"></script>
	<?php } ?>
</body>
</html>

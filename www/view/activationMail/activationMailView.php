<?php $title = "Activate your account"; ?>
<?php $style = "public/css/activationMailView.css" ?>

<?php ob_start() ?>

<div class="body" id="activateBody">
	<h2>Check your email!</h2>
	<p>An activation mail has been sent.</p>
	<p>Click on the link in the mail to activate your account.</p>
	<p>Haven't received any mail ?</p>
	<form action="/" method="post">
		<input type="submit" name="sendmail" value="Send me another one" />
	</form>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

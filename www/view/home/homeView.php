<?php $title = "Home"; ?>
<?php $ajaxFunctions = "public/js/ajaxFunctions.js"; ?>
<?php $js = "public/js/homeView.js"; ?>
<?php $style = "public/css/homeView.css"; ?>
<?php $header = "view/home/homeHeaderView.php"; ?>

<?php ob_start() ?>

<div class="body" id="homeBody">
	<div id="pictureDetail" style="display: none;">
		<div class="card">
			<div id="pictureCard">
			</div>
		</div>
	</div>
	<div class="home" id="home">
		<div id="noPicture" style="display: none;">
			<?php noPicture() ?>
		</div>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

<?php $connected = isConnected() && isUserActive() ?>

<div class="header">
	<div class="titleDiv">
		<form class="titleForm" action="/" method="post">
			<button class="titleButton" name="header" value="home" type="submit">
				<p class="titleP"><?= $title ?></p>
			</button>
		</form>
	</div>
	<form class="rightForm" action="/" method="post">
		<?php if ($connected) { ?>
			<button class="cameraButton" name="header" value="camera" type="submit">
				<img src="public/imgs/tools/camera-logo.png" alt="camera" title="Take new photo">
			</button>
			<button class="profileButton" name="header" value="profile" type="submit">
				<img src="public/imgs/tools/profile.png" alt="profile" title="Your profile">
			</button>
			<button class="logoutButton" name="header" value="logout" type="submit">
				<img src="public/imgs/tools/logout.png" alt="logout" title="Logout">
			</button>
		<?php } else { ?>
			<button class="loginButton" name="login" value="login" type="submit">
				<img src="public/imgs/tools/login.png" alt="login" title="Login">
			</button>
		<?php } ?>
	</form>
	<div class="rightSmallForm">
		<li>
			<img src="public/imgs/tools/menu.png" alt="menu" title="Menu" />
			<ul>
				<form action="/" method="post" class="innerUl">
					<?php if ($connected) { ?>
						<button type="submit" name="header" value="camera">
							<img src="public/imgs/tools/camera-logo-accent.png" alt="camera" title="Take new photo">
							<span>New photo</span>
						</button>
						<button type="submit" name="header" value="profile">
							<img src="public/imgs/tools/profile-accent.png" alt="profile" title="Profile">
							<span>Profile</span>
						</button>
						<button type="submit" name="header" value="logout">
							<img src="public/imgs/tools/logout-accent.png" alt="logout" title="Logout">
							<span>Logout</span>
						</button>
					<?php } else { ?>
						<button type="submit" name="header" value="logout">
							<img src="public/imgs/tools/logout-accent.png" alt="logout" title="Logout">
							<span>Logout</span>
						</button>
					<?php } ?>
				</form>
			</ul>
		</li>
	</div>
</div>

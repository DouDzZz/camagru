<?php $title = "Send activation mail"; ?>
<?php $style = "public/css/resendEmail.css"; ?>

<?php ob_start() ?>

<div class="body" id="resendEmailBody">
	<h2>Indicate the email address you signed up with:</h2>
	<form action="/" method="post">
		<input type="email" name="email" id="email" placeholder="Email">
		<input type="submit" name="resend" value="Send">
	</form>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

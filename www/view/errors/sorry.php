<?php $title = "Sorry" ?>
<?php $style = "public/css/sorry.css" ?>

<?php ob_start() ?>

<div class="body">
	<div class="sorry">
		<h1>Sorry...</h1>
		<p>
			We are deeply sorry, but an error occured
		</p>
	</div>
</div>

<?php $content = ob_get_clean(); ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

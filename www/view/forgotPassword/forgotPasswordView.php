<?php $title = "Forgot Password" ?>
<?php $style = "public/css/forgotPasswordView.css" ?>

<?php ob_start() ?>

<div class="body">
	<div class="formDiv">
		<form class="forgotForm" action="/" method="post">
			<h3>Forgot Password</h3>
			<div class="inputDiv">
				<input type="text" name="email" id="forgotEmail" autofocus placeholder="Email" autocomplete="email" />
			</div>
			<?php if (isset($_POST['nomail'])) { ?>
				<span class="errorMsg">We cannot find any user registered with <strong><?= $_POST['email'] ?></strong> in our database </span>
			<?php } ?>
			<div class="buttonDiv">
				<button id="forgotBtn" type="submit" name="resend" value="password">Send me an email</button>
				<button id="backBtn" type="submit" name="changeForm" value="login" >Back to login</button>
			</div>
		</form>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require("view/templates/template.php"); ?>

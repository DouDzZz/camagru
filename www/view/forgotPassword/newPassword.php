<?php $title = "New Password" ?>
<?php $header = "view/forgotPassword/newPasswordHeader.php" ?>
<?php $style = "public/css/newPassword.css" ?>

<?php ob_start() ?>

<div class="body">
	<div class="newPasswordDiv">
		<form class="newPasswordForm" action="/" method="post" >
			<h3>Reset Password</h3>
			<div class="inputDiv">
				<input type="password" name="password" autofocus placeholder="New Password" autocomplete="new-password" >
				<input type="password" name="confirm" placeholder="Confirm Password" autocomplete="new-password" >
				<?php if ($_GET['nomail'] == "true") { ?>
					<span class="errorMsg">We cannot find any user registered with <strong><?= $_GET['retrieve'] ?></strong> in our database</span>
				<?php } ?>
				<input style="display: none" name="email" value="<?= $_GET['retrieve'] ?>">
			</div>
			<div class="buttonDiv">
				<button type="submit" name="changePassword" value="password" >Change Password</button>
			</div>
		</form>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

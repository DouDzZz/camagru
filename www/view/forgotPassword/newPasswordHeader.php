<div class="header">
	<div class="titleDiv">
		<form class="titleForm" action="/" method="post">
			<button class="titleButton" name="header" value="home" type="submit">
				<p class="titleP"><?= $title ?></p>
			</button>
		</form>
	</div>
	<form class="rightForm" action="/" method="post">
		<?php if ($connected) { ?>
			<button class="logoutButton" name="header" value="home" type="submit">
				<img src="public/imgs/tools/home.png" alt="home" title="Home">
			</button>
		<?php } else { ?>
			<button class="logoutButton" name="header" value="home" type="submit">
				<img src="public/imgs/tools/home.png" alt="home" title="Home">
			</button>
		<?php } ?>
	</form>
	<div class="rightSmallForm">
		<li>
			<img src="public/imgs/tools/menu.png" alt="menu" title="Menu" />
			<ul>
				<form action="" method="post" class="innerUl">
					<button type="submit" name="header" value="home">
						<img src="public/imgs/tools/home.png" alt="home" title="Home">
						<span>Home</span>
					</button>
				</form>
			</ul>
		</li>
	</div>
</div>

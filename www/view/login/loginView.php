<?php $title = "Login" ?>
<?php $style = "public/css/loginView.css"; ?>

<?php if (session_status() != PHP_SESSION_ACTIVE) session_start() ?>

<?php $pseudo = htmlspecialchars($_SESSION['pseudo']) ?>
<?php $exists = htmlspecialchars($_SESSION['exists']) ?>

<?php ob_start() ?>

<div class="body">
	<div class="formDiv">
		<form class="loginForm" action="/" method="post">
			<h3>Login</h3>
			<div class="inputDiv">
				<input class="<?= $inputClasses ?> <?= $exists ?>" type="text" name="pseudo" value="<?= $pseudo ?>" id="pseudo" autofocus placeholder="Pseudo" autocomplete="username" />
				<span class="errorMsg"><?= $pseudoMsg ?></span>
				<input class="<?= $inputClasses ?>" type="password" name="password" id="password" minlength="6" placeholder="Password" autocomplete="current-password" />
				<span class="errorMsg"><?= $passwordMsg ?></span>
			</div>
			<div class="buttonDiv">
				<button id="loginBtn" name="login" value="login" type="submit">Login</button>
				<button id="signupBtn" type="submit" value="signup" class="changeForm" name="changeForm" value="signup">
					No account? Signup!
				</button>
				<button id="forgotBtn" type="submit" name="forgotPassword">Forgot password</button>
			</div>
		</form>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php");

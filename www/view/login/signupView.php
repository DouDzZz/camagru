<?php $title = "Login" ?>
<?php $style = "public/css/loginView.css"; ?>

<?php if (session_status() != PHP_SESSION_ACTIVE) session_start() ?>

<?php $pseudo = htmlspecialchars($_SESSION['pseudo']) ?>
<?php $email = htmlspecialchars($_SESSION['email']) ?>
<?php $exists = htmlspecialchars($_SESSION['exists']) ?>

<?php ob_start() ?>

<div class="body">
	<div class="formDiv">
		<form class="loginForm" action="/" method="post">
			<h3>Signup</h3>
			<div class="inputDiv">
				<input
					class="<?= $inputClasses ?> <?= $exists ?>"
					type="text"
					name="pseudo"
					value="<?= $pseudo ?>"
					id="pseudo"
					placeholder="Pseudo"
					autocomplete="username" />
				<span class="errorMsg"><?= $pseudoMsg ?></span>
				<input
					class="<?= $inputClasses ?>"
					type="email"
					name="email"
					value="<?= $email ?>"
					id="email"
					placeholder="Email"
					autocomplete="email" />
				<span class="errorMsg"><?= $emailMsg ?></span>
				<input
					class="<?= $inputClasses ?>"
					type="password"
					minlength="6"
					name="password"
					id="password"
					placeholder="Password"
					autocomplete="new-password" />
				<span class="errorMsg"><?= $passwordWeak ?></span>
				<input
					class="<?= $inputClasses ?>"
					type="password"
					minlength="6"
					name="confirm"
					id="confirm"
					placeholder="Confirm Password"
					autocomplete="new-password" />
				<span class="errorMsg"><?= $confirmWeak ?></span>
			</div>
			<div class="buttonDiv">
				<button name="signup" value="signup" type="submit" id="signupBtn">Signup</button>
				<button type="submit" value="login" class="changeForm" id="loginBtn"
					name="changeForm" value="login">
					Already have an account? Login!
				</button>
			</div>
		</form>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

<?php $title = "Profile" ?>
<?php $style = "public/css/profileView.css" ?>
<?php $js = "public/js/profileView.js" ?>
<?php $header = $_SERVER['DOCUMENT_ROOT']."/view/profile/profileHeaderView.php" ?>

<?php ob_start() ?>

<div class="body">
	<h1>Hello <?= $pseudo ?>!</h1>
	<div class="forms">
		<form class="pictureForm" action="/" method="post" id="pictureForm" enctype="multipart/form-data">
			<input type="file" name="inputfile" class="inputfile" id="file" size="10">
			<label for="file">
				<img class="profilePicture" id="profilePictureId" src="<?= $picture ?>" alt="profilePicture" title="Profile Picture" />
				<button style="display: none" type="submit" name="img" value="inputfile" id="pushButton">Confirm</button>
				<button style="display: none" type="button" id="cancelButton">Cancel</button>
			</label>
		</form>
		<div class="darkMode">
			<label for="radioButton">Dark Mode: </label>
			<div id="radioButton">
				<input type="checkbox" name="darkMode" id="darkModeSwitcher" <?php echo getDarkButton() === "true" ? 'checked' : '' ?>>
				<span class="slider round"></span>
			</div>
		</div>
		<div class="darkMode">
			<label for="radioButtonMail">Mail Notifications: </label>
			<div id="radioButtonMail" title="check this box to receive comments and like update by mail">
				<input type="checkbox" name="mailSwitch" id="mailSwitcher" <?php echo getMailPreference() === "true" ? 'checked' : '' ?>>
				<span class="slider round"></span>
			</div>
		</div>
		<div class="dataForm">
			<div class="pseudo">
				<label for="changePseudo">Change your pseudo:</label>
				<input type="text" name="changePseudo" id="changePseudo" value="<?= $pseudo ?>"/>
			</div>
			<div class="email">
				<label for="changeEmail">Change your email:</label>
				<input type="email" name="changeEmail" id="changeEmail" value="<?= $email ?>"/>
			</div>
			<div class="password">
				<label for="changePassword">Change your password:</label>
				<input type="password" name="changePassword" id="changePassword" placeholder="Password"/>
			</div>
			<div class="password">
				<label for="changeConfirm">Confirm your new password:</label>
				<input type="password" name="changeConfirm" id="changeConfirm" placeholder="Confirm" />
			</div>
			<button id="saveProfile" class="saveProfile" >Save Changes</button>
			<p>** Left data untouch to not change them **</p>
		</div>
		<div class="confirmPassword" style="display: none;">
			<div class="confirmPasswordForm">
				<h2>Log again to continue</h2>
				<input type="password" name="oldPassord" id="oldPassword" placeholder="Password">
				<button id="continueButton">Continue</button>
				<button id="backButton">Cancel</button>
			</div>
		</div>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

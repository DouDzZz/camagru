<div class="header">
	<div class="titleDiv">
		<form class="titleForm" action="/" method="post">
			<button class="titleButton" name="header" value="home" type="submit">
				<p class="titleP"><?= $title ?></p>
			</button>
		</form>
	</div>
	<form class="rightForm" action="/" method="post">
		<button class="homeButton" name="header" value="home" type="submit">
			<img src="public/imgs/tools/home.png" alt="home" title="Home page">
		</button>
		<button class="profileButton" name="header" value="profile" type="submit">
			<img src="public/imgs/tools/profile.png" alt="profile" title="Your profile">
		</button>
		<button class="logoutButton" name="header" value="logout" type="submit">
			<img src="public/imgs/tools/logout.png" alt="logout" title="Logout">
		</button>
	</form>
	<div class="rightSmallForm">
		<li>
			<img src="public/imgs/tools/menu.png" alt="menu" title="Menu" />
			<ul>
				<form action="/" method="post" class="innerUl">
					<button type="submit" name="header" value="home">
						<img src="public/imgs/tools/home-accent.png" alt="camera" title="Take new photo">
						<span>Home</span>
					</button>
					<button type="submit" name="header" value="profile">
						<img src="public/imgs/tools/profile-accent.png" alt="profile" title="Profile">
						<span>Profile</span>
					</button>
					<button type="submit" name="header" value="logout">
						<img src="public/imgs/tools/logout-accent.png" alt="logout" title="Logout">
						<span>Logout</span>
					</button>
				</form>
			</ul>
		</li>
	</div>
</div>

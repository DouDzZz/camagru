<?php $title = "Take photo" ?>
<?php $style = "public/css/cameraView.css" ?>
<?php $js = "public/js/cameraView.js" ?>
<?php $jsFunctions = "public/js/cameraViewFunctions.js" ?>
<?php $header = "view/camera/cameraHeaderView.php" ?>
<?php $visuPicture = file_exists("view/camera/visuPicture.html") ? file_get_contents("view/camera/visuPicture.html") : "" ?>

<?php ob_start() ?>

<div class="body">
	<?= $visuPicture ?>
	<div class="main">
		<div class="pictureProposal">
			<label for="pictureCategory">Choose a category:</label>
			<div class="selectDiv">
				<select name="pictureCategory" id="pictureCategory">
					<?php getCategory(); ?>
				</select>
			</div>
			<ul class="pictureGrid">
				<?php getPictureGrid() ?>
			</ul>
		</div>
		<div id="innerMain" class="innerMain">
			<video autoplay="true" title="Click to take a picture" id="videoElement"></video>
			<button id="startButton" title="Click to take a picture">Take Picture</button>
			<input type="file" style="display: none" id="browsePicture" title="Click to browse picture" />
			<label for="browsePicture" name="browsePicture" id="browseLabel">Browse Picture</label>
		</div>
		<div class="innerSide">
			<?php getUserPictures(); ?>
		</div>
	</div>
	<script src="public/js/customSelect.js"></script>
</div>

<?php $content = ob_get_clean() ?>

<?php require_once($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

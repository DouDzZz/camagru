<?php $title = "About" ?>
<?php $style = "public/css/aboutView.css" ?>

<?php ob_start() ?>

<div class="body">
	<div class="aboutBody">
		<h1 class="aboutTitle">Camagru</h1>
		<h3 class="aboutSubtitle">Created by edjubert</h3>
		<p>Camagru is the first 42 web project.
		<br>The project purpose is to create a small web application to create basic photo montage.
		<br>All pictures have to be public, likable and commentable
		<br>Here is the <a href="https://gitlab.com/DouDzZz/camagru">Gitlab</a> project page</p>
	</div>
</div>

<?php $content = ob_get_clean() ?>

<?php require($_SERVER['DOCUMENT_ROOT']."/view/templates/template.php"); ?>

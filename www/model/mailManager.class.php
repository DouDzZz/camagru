<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/dbConnect.class.php");

class MailManager extends DbConnect {
	public static	$verbose = false;

	public function		__construct() {
		if (self::$verbose === true)
			echo "MailManager(MODEL) class constructed".PHP_EOL;
		return ;
	}

	public function		__destruct() {
		if (self::$verbose === true)
			echo "MailManager(MODEL) class destructed".PHP_EOL;
		return ;
	}

	private function	_createNotificationMail($pictureId) {
		if (self::$verbose === true)
			echo "MailManager->_createCommonMail function called".PHP_EOL;
		$message = "<html><body style='width: 500px; height: 200px; border-radius: 5px; text-align: center;'>";
		$message .= "<div style='width: 500px; height: 50px; border-top-left-radius: 5px; text-align: center;";
		$message .= " border-top-right-radius: 5px; background: linear-gradient(80deg, #f52798, #fa7202)'>";
		$message .= "<h1 style='font-family: sans-serif; color: #fff'>Welcome to Camagru</h1>";
		$message .= "</div>";
		$message .= "<div style='padding-top: 10px; height: 300px; margin-bottom: 20px; padding-bottom: 10px; background-color: #f3f3f3; width: 500px;";
		$message .= "border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; border-bottom: solid;";
		$message .= "border-left: solid; border-right: solid; border-color: #cfcfcf; border-width: 1px'>";

		$message .= "<p>You have a new notification</p>";
		$message .= "<p>Hi there, somebody comment or like on one of you photo!</p>";
		$message .= "<p>Go check this out !</p>";
		$message .= "<form action='https://".$_SERVER['HTTP_HOST']."' method='post'>";
		$message .= "<button type='submit' name='commentMail' value='$pictureId'";
		$message .= " style='width: 300px; height: 50px; border-radius: 5px;";
		$message .= "background: linear-gradient(80deg, #f52798, #fa7202); color: #fff; font-size: 16px;";
		$message .= "font-weight: bold; text-transform: uppercase;'";
		$message .= ">Here we go!</button>";

		$message .= "</form>";
		$message .= "</div>";
		$message .= "<div style='height: 100px;>edjubert</div>";
		$message .= "</body></html>";
		return $message;
	}

	public function		sendNotificationMail($pictureId) {
		if (self::$verbose === true)
			echo "MailManager->sendNotificationMail function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureIdDB = $db->quote($pictureId);
			$getUserSql = "SELECT `email`, `user_hash` FROM Users WHERE `id` = (SELECT `user_id` FROM Pictures WHERE `id` = $pictureIdDB)";
			$userCmd = $db->prepare($getUserSql);
			$userCmd->execute();
			$ret = $userCmd->fetch(PDO::FETCH_ASSOC);
			echo $this.getMailPreference($ret['user_hash']);
			$dbPref = $this->_getDB();
			$userHashDB = $db->quote($ret['user_hash']);
			$sqlPref = "SELECT `preference` FROM MailPreference WHERE `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDB)";
			$cmdPref = $db->prepare($sqlPref);
			$cmdPref->execute();
			$retPref = $cmdPref->fetch(PDO::FETCH_ASSOC);
			if ($ret['email'] && $retPref['preference'] == true) {
				$subject = "Camagru - New Notification!";
				$content .= $this->_createNotificationMail($pictureId);
				$sender = "From: Contact\n";
				$sender .= "MIME-Version: 1.0\r\n";
				$sender .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$mailRet = mail($ret['email'], $subject, $content, $sender);
			}
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot send email (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		changeMailPreference($preference, $userHash) {
		if (self::$verbose === true)
			echo "MailManager->changeMailPreference function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashDB = $db->quote($userHash);
			$preferenceDB = $db->quote($preference);
			$sql = "UPDATE `MailPreference` SET `preference` = $preferenceDB WHERE `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDB)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->rowCount();
			return $ret;
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot change mail preference (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		getMailPreference($userHash) {
		if (self::$verbose === true)
			echo "MailManager->getMailPreference function called";
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashDB = $db->quote($userHash);
			$sql = "SELECT `preference` FROM MailPreference WHERE `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDB)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			return $ret['preference'];
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get mail preference (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		__toString() {
		if (file_exists("doc/mailManager.doc.txt"))
			return file_get_contents("doc/mailManager.doc.txt");
		return "MailManager(MODEL) class".PHP_EOL;
	}
}

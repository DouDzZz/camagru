<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/customException.class.php");

class 	DbConnect {
	public static	$verbose = false;
	private			$_host = "db";
	private			$_user = "edjubert";
	private			$_pass = "edjubert";
	private			$_name = "camagru_db";

	public function	__construct()
	{
		if (self::$verbose === true)
			echo "DbConnect class contructed".PHP_EOL;
		return ;
	}

	protected function	_getDB() {
		try {
			$db = new PDO("mysql:host=".$this->_host.";dbname=".$this->_name,
				$this->_user, $this->_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_FOUND_ROWS => true));
			return $db;
		}
		catch (PDOException $e) {}
	}

	public function	__destruct()
	{
		if (self::$verbose === true)
			echo "DbConnect class destructed".PHP_EOL;
		return ;
	}

	public function	__toString()
	{
		if (file_exists("doc/DbConnect.doc.txt"))
			return file_get_contents("doc/DbConnect.doc.txt");
		return "DbConnect class".PHP_EOL;
	}
}

<?
require_once($_SERVER['DOCUMENT_ROOT']."/model/dbConnect.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/customException.class.php");

class LoginManager extends DbConnect {
	public static	$verbose = false;

	public function		__construct() {
		if (self::$verbose === true)
			echo "LoginManager(MODEL) class constructed".PHP_EOL;
		return ;
	}

	public function		getUserHash($userHash) {
		if (self::$verbose === true)
			echo "LoginManager->getUserHash function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashQuote = $db->quote($userHash);
			$sql = "SELECT user_hash FROM Users WHERE `user_hash` = $userHashQuote";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret['user_hash'] === $userHash)
				return ($ret['user_hash']);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get user hash (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		logCheck($userHash, $pass) {
		if (self::$verbose === true)
			echo "LoginManager->logCheck function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashDB = $db->quote($userHash);
			$passDB = $db->quote(hash('whirlpool', $pass));
			$sql = "SELECT `id` FROM Users WHERE `user_hash` = $userHashDB AND `password` = $passDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret['id'])
				return (1);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot log check (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		changeTheme($theme, $userHash) {
		if (self::$verbose === true)
			echo "LoginManager->changeTheme function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$theme = $db->quote($theme);
			$userHash = $db->quote($userHash);
			$sql = "UPDATE `Dark` SET `dark` = $theme WHERE `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHash)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot change theme (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		getDarkMode($userHash) {
		if (self::$verbose === true)
			echo "LoginManager->getDarkMode function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHash = $db->quote($userHash);
			$sql = "SELECT `dark`, Dark.user_id AS 'id' FROM Dark WHERE `user_id` = (SELECT id FROM Users WHERE `user_hash` = $userHash)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if (isset($ret['id']) && $ret['id'] == 1)
				return (2);
			elseif (isset($ret['dark']))
				return ($ret['dark']);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get dark mode (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		checkUserHash($userHash) {
		if (self::$verbose === true)
			echo "LoginManager->checkUserHash function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashQuote = $db->quote($userHash);
			$sql = "SELECT user_hash, active FROM Users WHERE `user_hash` = $userHashQuote";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret['user_hash'] === $userHash && $ret['active'] == 1)
				return (1);
			elseif ($ret['user_hash'] === $userHash && $ret['active'] == 0)
				return (2);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot check user hash (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		checkUserEmail($email) {
		if (self::$verbose === true)
			echo "LoginManager->checkUserEmail function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$email = $db->quote($email);
			$sql = "SELECT `email` FROM Users WHERE `email` = $email";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if (isset($ret['email']) && $db->quote($ret['email']) == $email) {
				return (1);
			}
			else {
				return (0);
			}
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot check user email (".$e->getCode()."): ".$e.getMessage());
		}
	}

	public function		updateProfile($userHash, $newPseudo, $newEmail, $newPassword) {
		if (self::$verbose === true)
			echo "LoginManager->updateProfile function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashDB = $db->quote($userHash);
			$pseudoDB = $db->quote($newPseudo);
			$emailDB = $db->quote($newEmail);
			$passwordDB = $db->quote($newPassword);
			$sql = "UPDATE Users SET `pseudo` = $pseudoDB, `email` = $emailDB, `password` = $passwordDB WHERE `user_hash` = $userHashDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			echo $cmd->rowCount();
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot update profile (".$e->getCode()."): ".$e.getMessage());
		}
	}

	public function		checkPseudoUnique($pseudo, $hashUser) {
		if (self::$verbose === true)
			echo "LoginManager->checkPseudoUnique function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pseudo = $db->quote($pseudo);
			$hashUser = $db->quote($hashUser);
			$sql = "SELECT * FROM Users WHERE `pseudo` = $pseudo AND `user_hash` != $hashUser";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			return ($ret);
		} catch (PDOException $e) {
			throw new PDOException("ERROR check pseudo unique(".$e->getCode()."): ".$e.getMessage());
		}
	}

	public function		getUserData($userHash) {
		if (self::$verbose === true)
			echo "LoginManager->getUserData function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashDB = $db->quote($userHash);
			$sql = "SELECT * FROM Users WHERE `user_hash` = $userHashDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			return ($ret);
		} catch (PDOException $e) {
			throw new PDOException("ERROR get user data (".$e->getCode()."): ".$e.getMessage());
		}
	}

	public function		isUserActive($userHash) {
		if (self::$verbose === true)
			echo "LoginManager->isUserActive function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHashDB = $db->quote($userHash);
			$sql = "SELECT * FROM Users WHERE `user_hash` = $userHashDB AND `active` = 1";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			return ($cmd->rowCount());
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get if user active (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		activateAccount($activationId) {
		if (self::$verbose === true)
			echo "LoginManager->activateAccount function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$activationId = $db->quote($activationId);
			$sql = "UPDATE Users SET `active` = 1 WHERE `activation` = $activationId";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->rowCount();
			if ($ret)
				return (1);
			return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR activate account (".$e->getCode()."): ".$e->getMessage());
		}
	}

	private function	createActivationMessage($activationId) {
		$message = "<html><body style='width: 500px; height: 200px; border-radius: 5px; text-align: center;'>";
		$message .= "<div style='width: 500px; height: 50px; border-top-left-radius: 5px; text-align: center;";
		$message .= " border-top-right-radius: 5px; background: linear-gradient(80deg, #f52798, #fa7202)'>";
		$message .= "<h1 style='font-family: sans-serif; color: #fff'>Welcome to Camagru</h1>";
		$message .= "</div>";
		$message .= "<div style='padding-top: 10px; height: 300px; margin-bottom: 20px; padding-bottom: 10px; background-color: #f3f3f3; width: 500px;";
		$message .= "border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; border-bottom: solid;";
		$message .= "border-left: solid; border-right: solid; border-color: #cfcfcf; border-width: 1px'>";
		$message .= "<p>One last step</p>";
		$message .= "<p>Please click on the link below to activate your account:</p>";
		$message .= "<form action='https://".$_SERVER['HTTP_HOST']."' method='get'>";
		$message .= "<button type='submit' name='activate' value='$activationId'";
		$message .= " style='width: 300px; height: 50px; border-radius: 5px;";
		$message .= "background: linear-gradient(80deg, #f52798, #fa7202); color: #fff; font-size: 16px;";
		$message .= "font-weight: bold; text-transform: uppercase;'";
		$message .= ">Activate my account</button>";
		$message .= "</form>";
		$message .= "</div>";
		$message .= "<div style='height: 100px;>edjubert</div>";
		$message .= "</body></html>";
		return ($message);
	}
	
	private function	createRetrievePassword($email, $activationId) {
		$message = "<html><body style='width: 500px; height: 200px; border-radius: 5px; text-align: center;'>";
		$message .= "<div style='width: 500px; height: 50px; border-top-left-radius: 5px; text-align: center;";
		$message .= " border-top-right-radius: 5px; background: linear-gradient(80deg, #f52798, #fa7202)'>";
		$message .= "<h1 style='font-family: sans-serif; color: #fff'>Welcome to Camagru</h1>";
		$message .= "</div>";
		$message .= "<div style='padding-top: 10px; height: 300px; margin-bottom: 20px; padding-bottom: 10px; background-color: #f3f3f3; width: 500px;";
		$message .= "border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; border-bottom: solid;";
		$message .= "border-left: solid; border-right: solid; border-color: #cfcfcf; border-width: 1px'>";
		$message .= "<p>Retrieve Password Procedure</p>";
		$message .= "<p>Hi there, you ask us to send you an email to retrieve your password</p>";
		$message .= "<p>If this is not from you, you can forgot about it, take a cup of coffee and chill</p>";
		$message .= "<form action='https://".$_SERVER['HTTP_HOST']."' method='get'>";
		$message .= "<input  name='activationId' value='$activationId' style='display: none'>";
		$message .= "<button type='submit' name='retrieve' value='$email'";
		$message .= " style='width: 300px; height: 50px; border-radius: 5px;";
		$message .= "background: linear-gradient(80deg, #f52798, #fa7202); color: #fff; font-size: 16px;";
		$message .= "font-weight: bold; text-transform: uppercase;'";
		$message .= ">Reset my password</button>";
		$message .= "</form>";
		$message .= "</div>";
		$message .= "<div style='height: 100px;>edjubert</div>";
		$message .= "</body></html>";
		return ($message);
	}

	public function		resendMail($email, $type) {
		if (self::$verbose === true)
			echo "LoginManager->resendMail function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$emailDB = $db->quote($email);
			$sql = "SELECT `activation` FROM Users WHERE `email` = $emailDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if (isset($ret['activation'])) {
				if ($type == "mail") {
					$subject = "Camagru - Activate your account";
					$content = $this->createActivationMessage($ret['activation']);
					$sender = "From: Contact\n";
					$sender .= "MIME-Version: 1.0\r\n";
					$sender .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$mailRet = mail($email, $subject, $content, $sender);
				}
				elseif ($type == "password") {
					$subject = "Camagru - Retrieve Password";
					$content = $this->createRetrievePassword($email, $ret['activation']);
					$sender = "From: Contact\n";
					$sender .= "MIME-Version: 1.0\r\n";
					$sender .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$mailRet = mail($email, $subject, $content, $sender);
				}
			}
			else {
				header("Location: /?nomail=true");
			}
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot resend email (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		sendActivationMail($userHash) {
		if (self::$verbose === true)
			echo "LoginManager->sendActivationMail function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHash = $db->quote($userHash);
			$getActivationId = "SELECT `activation`, `email` FROM Users WHERE user_hash = $userHash";
			$cmd = $db->prepare($getActivationId);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			$activationId = $ret['activation'];
			$email = $ret['email'];
			if ($activationId) {
				$subject = "Camagru activation mail";
				$mailContent = $this->createActivationMessage($activationId);
				$sender = "From: Sender\n";
				$sender .= "MIME-Version: 1.0\r\n";
				$sender .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$mailRet = mail($email, $subject, $mailContent, $sender);
			}
		} catch (PDOException $e) {
			throw new PDOException("ERROR send activation mail(".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		signup($pseudo, $email, $password) {
		if (self::$verbose === true)
			echo "LoginManager->($ function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pseudoDB = $db->quote($pseudo);
			$emailDB = $db->quote($email);
			$password = $db->quote(hash('whirlpool', $password));
			$sql = "SELECT pseudo, email FROM Users WHERE `pseudo` = $pseudoDB OR `email` = $emailDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret['pseudo'] == $pseudo) {
				return (-1);
			}
			elseif ($ret['email'] == $email) {
				return (-2);
			}
			elseif ($ret) {
				return (0);
			}
			else {
				try {
					$timestamp = time();
					$hash_user = hash('whirlpool', $pseudo.strval($timestamp));
					$hash_user_quote = $db->quote($hash_user);
					$activation = $db->quote(hash('whirlpool', mt_rand(10000,99999).time().$email));
					$pushSql = "INSERT INTO "
						."Users (`pseudo`, `email`, `password`, `user_hash`, `active`, `activation`) "
						."VALUES ($pseudoDB, $emailDB, $password, $hash_user_quote, 0, $activation)";
					$pushCmd = $db->prepare($pushSql);
					$pushRet = $pushCmd->execute();
					$pushDark = "INSERT INTO Dark (`user_id`, `dark`) VALUES ((SELECT `id` FROM Users WHERE `user_hash` = $hash_user_quote), 0)";
					$cmd = $db->prepare($pushDark);
					$cmd->execute();
					$pushMailPref = "INSERT INTO MailPreference (`user_id`, `preference`) VALUES ((SELECT `id` FROM Users WHERE `user_hash` = $hash_user_quote), 1)";
					$cmdMail = $db->prepare($pushMailPref);
					$cmdMail->execute();
					if ($pushRet) {
						unset($_POST);
						$this->sendActivationMail($hash_user);
						return ($hash_user);
					}
				}
				catch (PDOException $e) {
					throw new Exception("Error while adding new user(".$e->getCode()."): ".$e->getMessage().PHP_EOL);
				}
				unset($_POST);
				return (0);
			}
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot signup (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		loginUser($pseudo, $password) {
		if (self::$verbose === true)
			echo "LoginManager->loginUser function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pseudo = $db->quote($pseudo);
			$password = $db->quote(hash('whirlpool', $password));
			$sql = "SELECT * FROM Users WHERE `pseudo` = $pseudo AND `password` = $password";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if (!$ret)
				return (0);
			else {
				return ($ret['user_hash']);
			}
		} catch (CustomException $e) {
			throw new CustomException();
		}
	}

	public function		__destruct() {
		if (self::$verbose === true)
			echo "LoginManager(MODEL) class destructed".PHP_EOL;
		return ;
	}

	public function		__toString() {
		if (file_exists("doc/LoginManager.doc.txt"))
			return file_get_contents("doc/LoginManager.doc.txt");
		return "LoginManager(MODEL)".PHP_EOL;
	}
}

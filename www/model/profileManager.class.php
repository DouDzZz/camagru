<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/dbConnect.class.php");

class ProfileManager extends DbConnect {
	public static	$verbose = false;

	public function		__construct() {
		if (self::$verbose === true)
			echo "ProfileManager(MODEL) class constructed".PHP_EOL;
		return ;
	}

	public function		isUserRoot($userHash) {
		if (self::$verbose === true)
			echo "ProfileManager->isUserRoot function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHash = $db->quote($userHash);
			$sql = "SELECT * FROM Admin JOIN Users ON Users.id = Admin.user_id WHERE Users.user_hash = $userHash";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			return $cmd->rowCount();
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot check if user is root (".$e->getCode()."): ".$e->getMessage());
		}
	} 

	public function		getProfilePicture($userHash) {
		if (self::$verbose === true)
			echo "ProfileManager->getProfilePicture function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHash = $db->quote($userHash);
			$sql = "SELECT profile_picture FROM Users WHERE `user_hash` = $userHash";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret)
				return ($ret);
			return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get profile picture (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		pushProfilePicture($imgPath, $userHash) {
		if (self::$verbose === true)
			echo "ProfileManager->pushProfilePicture function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$imgPath = $db->quote($imgPath);
			$userHash = $db->quote($userHash);
			$sql = "UPDATE Users SET `profile_picture` = $imgPath WHERE `user_hash` = $userHash";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->rowCount();
			$err = $cmd->errorInfo();
			return ($ret);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot push profile picture (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		profileData($userHash) {
		if (self::$verbose === true)
			echo "ProfileManager->userData function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$userHash = $db->quote($userHash);
			$sql = "SELECT pseudo, email, profile_picture FROM Users WHERE `user_hash` = $userHash";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret)
				return ($ret);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get profile data (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		__destruct() {
		if (self::$verbose === true)
			echo "ProfileManager(MODEL) class destructed".PHP_EOL;
		return ;
	}

	public function		__toString() {
		if (file_exists("doc/profileManager.doc.txt"))
			return file_get_contents("doc/profileManager.doc.txt");
		return "ProfileManager(MODEL) class".PHP_EOL;
	}
}

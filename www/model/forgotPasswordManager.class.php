<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/dbConnect.class.php");

class ForgotPasswordManager extends DbConnect {
	public static	$verbose = false;

	public function	__construct() {
		if (self::$verbose === true)
			echo "ForgotPasswordManager(MODEL) contructed".PHP_EOL;
	}

	public function	__destruct() {
		if (self::$verbose === true)
			echo "ForgotPasswordManager(MODEL) contructed".PHP_EOL;
	}

	public function	setNewPassword($email, $password) {
		if (self::$verbose === true)
			echo "ForgotPasswordManager->setNewPassword function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$passwordDB = $db->quote(hash('whirlpool', $password));
			$emailDB = $db->quote($email);
			$sql = "UPDATE `Users` SET `password` = $passwordDB WHERE `email` = $emailDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->rowCount();
			if($ret > 0)
				return (1);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot set new password (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function	__toString() {
		$filePath = $_SERVER['DOCUMENT_ROOT']."/doc/forgotPasswordManager.doc.txt";
		if (file_exists($filePath))
			return file_get_content($filePath);
		return "forgotPasswordManager class".PHP_EOL;
	}
}

<?php

class CustomException extends PDOException {
	public static	$verbose = false;

	public function	__construct($message=null, $code=null) {
		if (self::$verbose === true)
			echo "CustomException class contructed".PHP_EOL;
		require_once($_SERVER['DOCUMENT_ROOT']."/view/errors/sorry.php");
	}
}

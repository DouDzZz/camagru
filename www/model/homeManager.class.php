<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/dbConnect.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/customException.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/profileController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/mailManager.class.php");

class HomeManager extends DbConnect {
	public static	$verbose = false;

	public function		__construct() {
		if (self::$verbose === true)
			echo "HomeManager(MODEL) class constructed".PHP_EOL;
		return ;
	}

	public function		sendComment($comment, $pictureId) {
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureIdDB = $db->quote($pictureId);
			$comment = $db->quote($comment);
			$userHash = $db->quote($_SESSION['connected']);
			$sql = "INSERT INTO Comments (`picture_id`, `user_id`, `comment`) VALUES ($pictureIdDB, (SELECT `id` FROM Users WHERE `user_hash` = $userHash), $comment)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			/* $mailManager = new MailManager(); */
			/* $mailManager->sendCommentMail($pictureId); */
		}
		catch (PDOException $e) {
			echo "Cannot send comment: (".$e->getCode().") ".$e->getMessage();
		}
	}

	public function		userLikedIt($pictureId) {
		if (self::$verbose === true)
			echo "HomeManager->likePicture function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureIdDB = $db->quote($pictureId);
			$userHashDB = $db->quote($_SESSION['connected']);
			$sql = "SELECT * FROM Likes WHERE `picture_id` = $pictureIdDB AND `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDB)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->rowCount();
			return $ret;
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get if user already liked this picture (".$e->getCode()."): ".$e->getMessage());
		}

	}

	public function		likePicture($pictureId) {
		if (self::$verbose === true)
			echo "HomeManager->likePicture function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureIdDB = $db->quote($pictureId);
			$userHashDB = $db->quote($_SESSION['connected']);
			$checkSql = "SELECT COUNT(*) AS 'already_liked' FROM Likes WHERE `picture_id` = $pictureIdDB AND `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDB)";
			$check = $db->prepare($checkSql);
			$check->execute();
			$already = $check->fetch(PDO::FETCH_ASSOC);
			if ($already['already_liked']) {
				$sql = "DELETE FROM `Likes` WHERE `picture_id` = $pictureIdDB AND `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDB)";
			}
			else {
				$sql = "INSERT INTO Likes (`user_id`, `picture_id`) VALUES ((SELECT `id` FROM Users WHERE `user_hash` = $userHashDB), $pictureIdDB)";
			}
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->rowCount();
			return ($ret);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot like picture (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		getLikes($pictureId) {
		if (self::$verbose === true)
			echo "HomeManager->getLikes function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureIdDB = $db->quote($pictureId);
			$sql = "SELECT COUNT(*) AS 'nb_of_likes' FROM Likes WHERE `picture_id` = $pictureIdDB";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			return $ret['nb_of_likes'];
		} catch (PDOException $e) {
			throw new PDOException("ERROR Cannot get number of likes (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		getNumberOfPictures() {
		if (self::$verbose === true)
			echo "HomeManager->getNumberOfPictures function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$sql = "SELECT COUNT(*) as 'nb_of_rows' FROM Pictures";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			return $ret['nb_of_rows'];
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get number of pictures (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		getComments($pictureId) {
		if (self::$verbose === true)
			echo "HomeManager->getComments function called".PHP_EOL;
		try {
			$arr = array();
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureId = $db->quote($pictureId);
			$sql = "SELECT Pictures.id, `comment`, `pseudo`, `user_hash`, Comments.timestamp AS `timestamp` FROM Comments JOIN Pictures ON Pictures.id = Comments.picture_id JOIN Users ON Users.id = Comments.user_id WHERE Comments.picture_id = $pictureId ORDER BY Comments.timestamp DESC";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			while ($comment = $cmd->fetch(PDO::FETCH_ASSOC)) {
				$profilePicture = getProfilePicture($comment['user_hash']);
				$comment["profile_picture"] = $profilePicture;
				array_push($arr, $comment);
			}
			if (count($arr) > 0)
				return ($arr);
			return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get comments (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		getPictures($offset = 0, $limit = 20) {
		if (self::$verbose === true)
			echo "HomeManager->getPictures function called".PHP_EOL;
		try {
			$ret = array();
			$db = $this->_getDB();
			if (!$db)
				return (0);
			if (!is_numeric($offset))
				$offset = 0;
			if (!is_numeric($limit))
				$limit = 12;
			$sql = "SELECT Pictures.id, `image_url`, `title`, `timestamp`, `pseudo`, (SELECT COUNT(*) FROM Comments WHERE Comments.picture_id = Pictures.id) AS nb_of_comments, (SELECT COUNT(*) FROM Likes WHERE Likes.picture_id = Pictures.id) AS nb_of_likes FROM Pictures JOIN Users ON Pictures.user_id = Users.id ORDER BY `timestamp` DESC LIMIT $offset, $limit";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			while ($picture = $cmd->fetch(PDO::FETCH_ASSOC)) {
				array_push($ret, $picture);
			}
			if (count($ret) > 0)
				return ($ret);
			return (0);
		} catch (PDOException $e) {
			echo json_encode(array(
				'error' => array(
					'code' => $e->getCode(),
					'msg' => $e->getMessage()
				)
			));
		}
	}

	public function		getUserData($userId) {
		if (self::$verbose === true)
			echo "HomeManager->getUserData function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$sql = "SELECT `pseudo` FROM Users WHERE `id` = $userId";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			if ($ret)
				return ($ret);
			else
				return (0);
		} catch (PDOException $e) {
			throw new PDOException("ERROR cannot get user data (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		__destruct() {
		if (self::$verbose === true)
			echo "HomeManager(MODEL) class destructed".PHP_EOL;
		return ;
	}

	public function		__toString() {
		if (file_exists("doc/homeManager.doc.txt"))
			return file_get_contents("doc/homeManager.doc.txt");
		return "HomeManager(MODEL) class".PHP_EOL;
	}
}

<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/dbConnect.class.php");

class CameraManager extends DbConnect {
	public static	$verbose = false;

	public function		__construct() {
		if (self::$verbose === true)
			echo "CameraManager(MODEL) class constructed".PHP_EOL;
		return ;
	}

	public function		deletePicture($pictureTimestamp) {
		if (self::$verbose === true)
			echo "CameraManager->deletePicture function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictureTimestamp = $db->quote(date('Y-m-d H:i:s', $pictureTimestamp));
			$sql = "DELETE FROM `Pictures` WHERE `timestamp` = $pictureTimestamp";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			$ret = $cmd->fetch(PDO::FETCH_ASSOC);
			return ($ret);
		} catch (PDOException $e) {
			throw new PDOException("ERROR while deleting picture(".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		postPhoto($path, $title, $userHash) {
		if (self::$verbose === true)
			echo "CameraManager->postPhoto function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pathDB = $db->quote($path);
			$titleDB = $db->quote($title);
			$userHashDb = $db->quote($userHash);
			$getUserId = "SELECT `id` FROM Users WHERE `user_hash` = $userHashDb";
			$user = $db->prepare($getUserId);
			$user->execute();
			$ret = $user->fetch(PDO::FETCH_ASSOC);
			$userIdDb = $db->quote($ret['id']);
			$sql = "INSERT INTO Pictures (`user_id`, `image_url`, `title`) VALUES ($userIdDb, $pathDB, $titleDB)";
			$cmd = $db->prepare($sql);
			$cmd->execute();
		}
		catch (PDOException $e) {
			throw new PDOException("ERROR while creating new picture(".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		userPictures($userHash) {
		if (self::$verbose === true)
			echo "CameraManager->userPictures function called".PHP_EOL;
		try {
			$db = $this->_getDB();
			if (!$db)
				return ;
			$pictures = array();
			$userHashDb = $db->quote($userHash);
			$sql = "SELECT * FROM Pictures WHERE `user_id` = (SELECT `id` FROM Users WHERE `user_hash` = $userHashDb) ORDER BY `timestamp` DESC";
			$cmd = $db->prepare($sql);
			$cmd->execute();
			if (!$cmd)
				return (0);
			else {
				while ($ret = $cmd->fetch(PDO::FETCH_ASSOC)) {
					array_push($pictures, $ret);
				}
				return ($pictures);
			}
		} catch (PDOException $e) {
			throw new PDOException("ERROR while seting up new Picture (".$e->getCode()."): ".$e->getMessage());
		}
	}

	public function		__destruct() {
		if (self::$verbose === true)
			echo "CameraManager(MODEL) class destructed".PHP_EOL;
		return ;
	}

	public function		__toString() {
		if (file_exists("doc/cameraManager.doc.txt"))
			return file_get_contents("doc/cameraManager.doc.txt");
		return "CameraManager(MODEL) class".PHP_EOL;
	}
}


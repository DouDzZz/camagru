<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/loginManager.class.php");



/*
**	changeTheme
**
**	changeTheme function 
**	@since 0.0.0
**	@param	Boolean	$theme		new theme
**	@param	String	$userHash	user hash to access preferences into database
*/
function	changeTheme($theme, $userHash) {
	if (isConnected()) {
		$loginManager = new LoginManager();
		if ($theme === "false")
			$loginManager->changeTheme(0, $userHash);
		else
			$loginManager->changeTheme(1, $userHash);
	}
}

/*
**	userDarkMode
**
**	userDarkMode function return user preference theme
**	@since 0.0.0
**	@param	String	$userHash	user hash to access preferences into database
**	@return	Boolean				the user preference
*/
function	userDarkMode($userHash) {
	if (isConnected() == 1) {
		$loginManager = new LoginManager();
		$ret = $loginManager->getDarkMode($userHash);
		if ($ret == 1)
			return ("true");
		elseif ($ret == 2)
			return ("root");
		else
			return ("false");
	}
}


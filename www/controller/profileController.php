<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/profileManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/loginManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/mailManager.class.php");

function	getProfilePage($err = 1) {
	$profileManager = new ProfileManager();
	$profileData = $profileManager->profileData($_SESSION['connected']);
	$pseudo = htmlspecialchars(htmlspecialchars_decode($profileData['pseudo']));
	$email = htmlspecialchars(htmlspecialchars_decode($profileData['email']));
	$err = $profileData;
	$picture = getProfilePicture($_SESSION['connected']);
	require_once($_SERVER['DOCUMENT_ROOT']."/view/profile/profileView.php");
}

function	isUserRoot() {
	$profileManager = new ProfileManager();
	$isRoot = $profileManager->isUserRoot($_SESSION['connected']);
	return $isRoot;
}

function	getDarkButton() {
	if (!isset($_SESSION['dark']))
		return ("false");
	else {
		return ($_SESSION['dark']);
	}
}

function	getMailPreference() {
	$mailManager = new MailManager();
	$mailPreference = $mailManager->getMailPreference($_SESSION['connected']);
	return $mailPreference ? "true" : "false";
}

function	getProfilePicture($userHash) {
	$profileManager = new ProfileManager();
	$profilePicture = $profileManager->getProfilePicture($userHash);
	if (!$profilePicture['profile_picture'])
		return "public/imgs/tools/default_profile.jpg";
	return ($profilePicture['profile_picture']);
}

function	postProfilePicture($input) {
	$loginManager = new LoginManager();
	$userHash = $loginManager->getUserHash($_SESSION['connected']);
	if (!$userHash)
		return (0);
	if ($input && $_FILES[$input])
		$fileData = $_FILES[$input];
	else
		return (-1);
	$_FILES[$input] = NULL;
	unset($_FILES[$input]);
	$absPath = "/var/www/html/";
	$relPath = "public/imgs/uploads/$userHash/profile_picture/";
	$targetDir = $absPath.$relPath;
	if (!file_exists($targetDir)) {
		mkdir($targetDir, 0777, true);
	}
	else {
		$files = glob($targetDir."*");
		foreach($files as $file) {
			if (is_file($file)) {
				unlink($file);
			}
		}
	}
	$uploadOK = 1;
	$targetFile = $targetDir.basename($fileData["name"]);
	$imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
	$fileName = hash('md5', $userHash.time()).".".$imageFileType;
	$newFile = $targetDir.$fileName;
	$check = getimagesize($fileData["tmp_name"]);
	if (file_exists($targetFile)) {
		$uploadOK = -1;
	}
	if ($fileData["size"] > 500000) {
		$uploadOK = -2;
	}
	if ($check !== false) {
		$uploadOK = 1;
	}
	else {
		$uploadOK = -4;
	}
	if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
		$uploadOK = -5;
	}
	if ($uploadOK != 1) {
		getProfilePage($uploadOK);
	}
	else {
		if (move_uploaded_file($fileData["tmp_name"], $newFile)) {
			$profileManager = new ProfileManager();
			$profileManager->pushProfilePicture($relPath.$fileName, $userHash);
			getProfilePage();
		}
		else {
			getProfilePage(-5);
		}
	}
}

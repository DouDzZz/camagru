<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/loginManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/themeController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/toolsController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/mailController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/homeController.php");


/*
**	postProfile
**
**	postProfile function change all user profile updates
**	@since 0.0.0
**	@param	String	$newPseudo		New user pseudo
**	@param	String	$newMail		New user mail
**	@param	String	$newPassword	New user password
**	@param	String	$newConfirm		New password confirmation
*/
function	postProfile($newPseudo, $newMail, $newPassword, $newConfirm) {
	$loginManager = new LoginManager();
	$userData = $loginManager->getUserData($_SESSION['connected']);
	if (!filter_var($newMail, FILTER_VALIDATE_EMAIL))
		$newMail = $userData['email'];
	if (strlen($newPseudo) < 3)
		$newPseudo = $userData['pseudo'];
	if (strlen($newPassword) < 6 || $newPassword !== $newConfirm)
		$newPassword = $userData['password'];
	else
		$newPassword = hash('whirlpool', htmlspecialchars_decode($newPassword));
	$loginManager->updateProfile($_SESSION['connected'], $newPseudo, $newMail, $newPassword);
}

/*
**	isUserActive
**
**	isUserActive function return if the user is active or not
**	@since 0.0.0
**	@return	Boolean	isActive
*/
function	isUserActive() {
	$loginManager = new LoginManager();
	$ret = $loginManager->isUserActive($_SESSION['connected']);
	return $ret;
}

/*
** getSignupPage
**
** getSignupPage function show the signup page
** @since 0.0.0
*/
function	getSignupPage($err) {
	$ret = checkSignupInput($_POST['pseudo'], $_POST['email'],
		$_POST['password'], $_POST['confirm']);
	$inputClasses = "inputForm";
	
	if (count($ret) === 0) {
		$_SESSION['pseudo'] = $_POST['pseudo'];
		$_SESSION['email'] = $_SESSION['email'];
		if ($err == "normal" && $_POST['password'] === $_POST['confirm']) {
			signupUser($_POST['pseudo'], $_POST['email'], $_POST['password']);
		}
		elseif ($err == "pseudo") {
			$pseudoMsg = "Pseudo already taken";
			require_once($_SERVER['DOCUMENT_ROOT']."/view/login/signupView.php");
		}
		elseif ($err == "email") {
			$emailMsg = "Email already Taken";
			require_once($_SERVER['DOCUMENT_ROOT']."/view/login/signupView.php");
		}
		else {
			require_once($_SERVER['DOCUMENT_ROOT']."/view/login/signupView.php");
		}
	}
	else {
		if ($ret && (in_array(-1, $ret) || in_array(-6, $ret)))
			$pseudoMsg = "Pseudo length must be between 4 and 20 char";
		if ($ret && in_array(-2, $ret))
			$emailMsg = "Email badly formatted";
		if ($ret && (in_array(-3, $ret) || in_array(-7, $ret)))
			$passwordWeak = "Password too weak: must have at least 6 char with lowercase, uppercase, numerical and special character";
		if ($ret && in_array(-4, $ret))
			$confirmWeak = "Password too weak: must have at least 6 char with lowercase, uppercase, numerical and special character";
		if ($ret && in_array(-5, $ret)) {
			$confirmWeak = "Passwords are differents";
			$passwordWeak = "Passwords are differents";
		}
		require_once($_SERVER['DOCUMENT_ROOT']."/view/login/signupView.php");
	}
}

/*
**	getLoginPage
**
**	getLoginPage function show the login page
**	@since 0.0.0
**	@param Boolean $err Tell if this function is called after a login failed attempt
*/
function	getLoginPage() {
	$inputClasses = "inputForm";
	if (isset($_POST['login']) && $_POST['login'] === "login") {
		$ret = checkLoginInput($_POST['pseudo'], $_POST['password']);
		if (count($ret) == 0) {
			$pseudo = $_POST['pseudo'];
			$password = $_POST['password'];
			unset($_POST);
			loginUser($pseudo, $password);
		}
		else {
			if ($ret && in_array(-1, $ret))
				$pseudoMsg = "Pseudo too short (min 4 char)";
			if ($ret && in_array(-2, $ret))
				$passwordMsg = "Password too weak (min 6 char, lowercase, uppercase, numerical, special)";
			require_once($_SERVER['DOCUMENT_ROOT']."/view/login/loginView.php");
		}
	}
	else {
		require_once($_SERVER['DOCUMENT_ROOT']."/view/login/loginView.php");
	}
}

/*
**	getAboutPage
**
**	getAboutPage function returns the about page
**	@since 0.0.0
*/
function	getAboutPage() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/about/aboutView.php");
}

/*
**	signupUser
**
**	signupUser create a new user account
**	@since 0.0.0
**	@param String $pseudo New user pseudo
**	@param String $email New user email
**	@param String $password New user password
*/
function	signupUser($pseudo, $email, $password) {
	$loginManager = new LoginManager();
	$logged = $loginManager->signup($pseudo, $email, $password);
	if (!$logged || $logged === -1 || $logged === -2) {
		$_POST['wrongForm'] = "all";
		if ($logged === -1)
			getSignupPage("pseudo");
		elseif ($logged === -2)
			getSignupPage("email");
		else
			getSignupPage("error");
		unset($_POST);
	}
	else  {
		$_SESSION['connected'] = $logged;
		$_SESSION['email'] = $email;
		$_SESSION['active'] = 0;
		if (isConnected() == 2)
			getEmailConfirmation();
		else
			getHomePage();
	}
}

/*
**	loginUser
**
**	loginUser login to user account
**	@since 0.0.0
**	@param String $pseudo New user pseudo
**	@param String $password New user password
*/
function	loginUser($pseudo, $password) {
	$loginManager = new LoginManager();
	$logged = $loginManager->loginUser($pseudo, $password);
	if (!$logged)
		getLoginPage();
	elseif (isConnected() == 2) {
		$_SESSION['active'] = '';
		$_SESSION['test'] = '';
		$_SESSION['connected'] = '';
		unset($_SESSION['email']);
		unset($_SESSION['active']);
		unset($_SESSION['test']);
		unset($_SESSION['connected']);
		logout();
		require_once($_SERVER['DOCUMENT_ROOT']."/view/activationMail/activationMailView.php");
	}
	else {
		$_SESSION['connected'] = $logged;
		$_SESSION['dark'] = userDarkMode($_SESSION['connected']);
		getHomePage();
	}
}

/*
**	logout
**
**	logout function log out the user
**	@since 0.0.0
*/
function	logout() {
	unset($_SESSION['connected']);
	$_SESSION['dark'] = "false";
	getLoginPage();
}



<?php
require_once($_SERVER['DOCUMENT_ROOT']."/controller/loginController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/cameraController.php");

function	uploadPhoto() {
	$uploadDb = "public/imgs/uploads/".$_SESSION['connected']."/";
	$uploadDir = $_SERVER['DOCUMENT_ROOT']."/".$uploadDb;
	if (!is_dir($uploadDir))
		mkdir($uploadDir, 0774);
	$img = $_POST["hidden_input"];
	$title = $_POST["picture_title"];
	$filter = $_SERVER['DOCUMENT_ROOT']."/".$_POST["filter_address"];
	$data = substr($img, strpos($img, ",") + 1);
	$pos  = strpos($img, ';');
	$type = explode('/', explode(':', substr($img, 0, $pos))[1])[1];
	$decodedData = base64_decode($data);
	$file = $uploadDir.mktime().".temp.".$type;
	$success1 = file_put_contents($file, $decodedData);
	if ($success1 && strlen($title) < 51) {
		list($width1, $height1) = getimagesize($file);
		list($width2, $height2) = getimagesize($filter);
		$imageResource  = imagecreatetruecolor($width1, $height1);
		$transparentColor  = imagecolorallocatealpha($imageResource, 0, 0, 0, 127);
		imagesavealpha($imageResource, true);
		imagefill($imageResource, 0, 0, $transparentColor);
		$final = hash('md5', $_SESSION['connected'])."_".mktime().".png";
		$finalDb = $uploadDb.$final;
		$finalDir = $uploadDir.$final;

		$newFilter = imagecreatefrompng($filter);
		if (strtolower($type) === "png")
			$newPhoto = imagecreatefrompng($file);
		elseif (strtolower($type) === "jpg" || strtolower($type) === "jpeg")
			$newPhoto = imagecreatefromjpeg($file);
		elseif (strtolower($type) === "gif")
			$newPhoto = imagecreatefromgif($file);
		else
			echo "File type not supported";

		if ($newPhoto) {
			imagecopyresampled($imageResource, $newPhoto, 0, 0, 0, 0, $width1, $height1, $width1, $height1);
			imagecopyresampled($imageResource, $newFilter, $width1 / 4, $height1 / 4, 0, 0, $width1 / 2, $height1 / 2, $width2, $height2);
			imagepng($imageResource, $finalDir);

			imagedestroy($imageResource);
			imagedestroy($newFilter);
			unlink($file);

			$cameraManager = new CameraManager();
			if (isConnected())
				$cameraManager->postPhoto($finalDb, $title, $_SESSION['connected']);
			else {
				echo "Unavailable to reach propre profile";
				logout();
			}
		}
		else {
			throw new Exception("File type not supported");
		}
	}
	else {
		print("ERROR: Cannot write temp file: ".strlen($title).PHP_EOL);
		if (file_exists($file))
			unlink($file);
	}
}

function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
	// creating a cut resource
	$cut = imagecreatetruecolor($src_w, $src_h);

	// copying relevant section from background to the cut resource
	imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);

	// copying relevant section from watermark to the cut resource
	imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

	// insert cut resource to destination image
	imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
}

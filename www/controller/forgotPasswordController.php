<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/forgotPasswordManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/toolsController.php");

function	getForgotPassword() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/forgotPassword/forgotPasswordView.php");
}

function	getNewPasswordPage() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/forgotPassword/newPassword.php");
}

function	getRetrieveAgain($email) {
	header("Location: /?retrieve=".urlencode($email)."&nomail=true");
}

function	setNewPassword($email, $password, $confirm) {
	$forgotManager = new ForgotPasswordManager();
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == $email
		&& $password && $confirm && $password === $confirm && checkPassStrength($password)) {
		$ret = $forgotManager->setNewPassword($email, $password);
		if ($ret === 1) {
			getLoginPage();
		}
		else {
			getRetrieveAgain($email);
		}
	}
	else {
		getRetrieveAgain($email);
	}
}

<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/homeManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/toolsController.php");

function	getHomePage() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/home/homeView.php");
}

function	noPicture() {
	if (file_exists("view/home/homeNoPicture.php"))
		echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/view/home/homeNoPicture.php");
	else
		echo "";
}

function	getLikes() {
	$homeManager = new HomeManager();
	$pictureId = $_POST['pictureId'];
	$likes = $homeManager->getLikes($pictureId);
	echo $likes;
}

function	userLikedIt($pictureId) {
	$homeManager = new HomeManager();
	$ret = $homeManager->userLikedIt($pictureId);
	echo $ret;
}

function	likePicture($pictureId) {
	$homeManager = new HomeManager();
	if  (isConnected())
		$ret = $homeManager->likePicture($pictureId);
	else
		$ret = 0;
	echo $ret;
}

function	sendComment() {
	$homeManager = new HomeManager();
	$comment = $_POST['sendComment'];
	$pictureId = $_POST['pictureId'];
	if (strlen($comment) > 0)
		$homeManager->sendComment($comment, $pictureId);
}

function	getComments() {
	$homeManager = new HomeManager();
	if (is_numeric($_POST['get_comments'])) {
		$pictureId = $_POST['get_comments'];
	}
	else
		throw new Exception("Error");
	$ret = $homeManager->getComments($pictureId);
	echo json_encode($ret);
}

function	getPictures() {
	$homeManager = new HomeManager();
	if (isset($_POST['offset']) && is_numeric($_POST['offset']))
		$offset = $_POST['offset'];
	else
		$offset = 0;
	if (isset($_POST['limit']) && is_numeric($_POST['limit']))
		$limit = $_POST['limit'];
	else
		$limit = 32;
	$pictures = $homeManager->getPictures($offset, $limit);
	if ($pictures)
		echo json_encode($pictures);
	else
		print(0);
}

function	getNbOfPictures() {
	$homeManager = new HomeManager();
	$nbOfPictures = $homeManager->getNumberOfPictures();
	echo $nbOfPictures;
}

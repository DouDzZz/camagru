<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/cameraManager.class.php");

function	getCameraPage() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/camera/cameraView.php");
}

function	getUserPictures() {
	$cameraManager = new CameraManager();
	$pictures = $cameraManager->userPictures($_SESSION['connected']);
	if ($pictures) {
		foreach ($pictures as $photo) {
			$timestamp = strtotime($photo['timestamp']);
			$title = htmlspecialchars($photo['title']);
			$imgUrl = $photo['image_url'];
			echo "<div class='userPicture'><div class='delDiv'></div>";
			echo "<img class='pictureImg' src='".$imgUrl."' id='".$timestamp."' title='".$title."' />";
			echo "</div>";
		}
	}
}

function	browsePicture() {
}

function	deletePicture() {
	$cameraManager = new CameraManager();
	$cameraManager->deletePicture($_POST['deletePicture']);
	$ret = $cameraManager->userPictures($_SESSION['connected']);
	echo json_encode($ret);
}

function	getCategory() {
	echo "<option value='all'>All</option>";
	foreach (glob("public/imgs/filters/*") as $foldername) {
		echo "<option value='".strtolower(basename($foldername))
			."'>".ucfirst(strtolower(basename($foldername)))
			."</option>";
	}
}

function	pushCanvas() {
	getCameraPage();
}

function	getPictureGrid() {
	foreach (glob("public/imgs/filters/*") as $foldername) {
		$folder = basename($foldername);
		foreach (glob("public/imgs/filters/$folder/*") as $picture) {
			$pathInfo = pathinfo($picture);
			if ($pathInfo['extension'] == 'png') {
				echo "<img class='filterThumbnail ".strtolower(basename($foldername))
					."' src='".$picture
					."' alt='".ucfirst(strtolower($pathInfo['filenamme']))
					."' title='".ucfirst(strtolower($pathInfo['filename']))
					."'>";
			}
		}
	}
}

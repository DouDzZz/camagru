<?php
require_once($_SERVER['DOCUMENT_ROOT']."/controller/loginController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/uploadPhoto.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/homeController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/cameraController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/mailController.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/themeController.php");

if (!session_status() != PHP_SESSION_ACTIVE)
	session_start();

try {
	if (isset($_POST['ajax']) && isset($_POST['isConnected']))
	{
		if (isConnected())
			echo json_encode(true);
		else
			echo json_encode(false);
	}
	if (isset($_POST['ajax']) && isConnected()) {
		if (isset($_POST['hidden_input']) && isset($_POST['picture_title'])
		&& isset($_POST['filter_address'])) {
			uploadPhoto();
		}
		elseif (isset($_POST['checkPseudoUnique'])) {
			checkPseudoUnique($_POST['checkPseudoUnique']);
		}
		elseif (isset($_POST['mailSwitch'])) {
			echo "salut";
			changeMailPreference($_POST['mailSwitch'], $_SESSION['connected']);
		}
		elseif (isset($_POST['switchDark'])) {
			changeTheme($_POST['switchDark'], $_SESSION['connected']);
		}
		elseif (isset($_POST['postNewProfile'])) {
			$newPseudo = htmlspecialchars($_POST['newPseudo']);
			$newMail = htmlspecialchars($_POST['newMail']);
			$newPassword = htmlspecialchars($_POST['newPassword']);
			$newConfirm = htmlspecialchars($_POST['newConfirm']);
			postProfile($newPseudo, $newMail, $newPassword, $newConfirm);
		}
		elseif (isset($_POST['logConfirm'])) {
			logCheck($_POST['logConfirm']);
		}
		elseif (isset($_POST['userLikedIt'])) {
			userLikedIt($_POST['pictureId']);
		}
		elseif (isset($_POST['getLikes'])) {
			getLikes($_POST['pictureId']);
		}
		elseif (isset($_POST['likePicture'])) {
			likePicture($_POST['pictureId']);
		}
		elseif (isset($_POST['get_comments'])) {
			getComments();
		}
		elseif (isset($_POST['deletePicture'])) {
			deletePicture();
		}
		elseif (isset($_POST['sendNotificationMail'])) {
			sendNotificationMail($_POST['pictureId']);
		}
		elseif (isset($_POST['sendComment'])) {
			sendComment();
		}
		elseif (isset($_POST['browsePicture'])) {
			browsePicture();
		}
		elseif (isset($_POST['getHomePictures'])) {
			getPictures();
		}
		elseif (isset($_POST['get_nb_of_pictures'])) {
			getNbOfPictures();
		}
	}
	elseif (isset($_POST['ajax']) && isset($_POST['getHomePictures'])) {
		getPictures();
	}
	elseif (!isset($_POST['ajax'])) {
		header("Location: /");
	}
}
catch (Exception $e) {
	throw new Exception("ERROR while responding to ajax request (".$e->getCode()."): ".$e->getMessage());
}

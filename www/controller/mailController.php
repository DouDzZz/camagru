<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/loginManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/mailManager.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/controller/loginController.php");

/*
**	changeMailPreference
**
**	changeMailPreference function change the mail preference in the database
**	@since	0.0.0
**	@param	mailPreference	The new mail preference to write into the database
**	@param	userHash		The user hash to fing the good place to write to the database
 */
function	changeMailPreference($mailPreference, $userHash) {
	$mailManager = new MailManager();
	if (isConnected()) {
		$mailManager = new MailManager();
		if ($mailPreference == "false")
			$mailManager->changeMailPreference(0, $userHash);
		else
			$mailManager->changeMailPreference(1, $userHash);
	}
}

/*
**	sendNotificationMail
**
**	sendNotificationMail function send the notification mail
**	@since	0.0.0
**	@param	$pictureId	the database id of the associated picture
 */
function	sendNotificationMail($pictureId) {
	$mailManager = new MailManager();
	$mailManager->sendNotificationMail($pictureId);
}

/*
**	activateAccount
**
**	activateAccount function checks the activation token
**	@since 0.0.0
*/
function	activateAccount() {
	$loginManager = new LoginManager();
	if (isset($_GET['activate']) && strlen($_GET['activate']) == 128) {
		$loginManager->activateAccount($_GET['activate']);
		unset($_SESSION['connected']);
		unset($_POST);
		header("Location: /");
	}
	else
		require_once($_SERVER['DOCUMENT_ROOT']."/view/errors/sorry.php");
}

/*
**	sendMail
**
**	sendMail function send mail to activate the new user account
**	@since 0.0.0
*/
function	sendMail() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/resendEmail/resendEmail.php");
}

/*
**	resendMail
**
**	resendMail function send again the mail to activate the account
**	@since 0.0.0
*/
function	resendMail($type) {
	$loginManager = new LoginManager();
	if ($loginManager->checkUserEmail($_POST['email'])) {
		if (!$type)
			$type = "mail";
		$email = $_POST['email'];
		unset($_POST['email']);
		$loginManager->resendMail($email, $type);
		require_once($_SERVER['DOCUMENT_ROOT']."/view/activationMail/activationMailView.php");
	}
	else {
		$_POST['nomail'] = true;
		getForgotPassword();
	}
}

/*
**	getEmailConfirmation
**
**	getEmailConfirmation function return the email confirmation page
**	@since 0.0.0
*/
function	getEmailConfirmation() {
	require_once($_SERVER['DOCUMENT_ROOT']."/view/activationMail/activationMailView.php");
}


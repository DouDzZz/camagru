<?php
require_once($_SERVER['DOCUMENT_ROOT']."/model/loginManager.class.php");


/*
**	checkPassStrength
**
**	checkPassStrength function check if the password is strong enought
**	@since 0.0.0
**	@param	String	$pass	the password to check
**	@return	Boolean			return true if the password is strong enough, false otherwise
*/
function		checkPassStrength($pass) {
	$spe = 0;
	$alph = 0;
	$maj = 0;
	$num = 0;
	$tmp = $pass;
	for ($i = 0; $i < strlen($pass); $i++) {
		if ($pass[$i] >= 'a' && $pass[$i] <= 'z')
			$alph++;
		elseif ($pass[$i] >= 'A' && $pass[$i] <= 'Z')
			$maj++;
		elseif ($pass[$i] >= '0' && $pass[$i] <= '9')
			$num++;
		else
			$spe++;
	}
	return ($spe > 0 && $alph > 0 && $maj > 0 && $num > 0);
}

/*
**	checkPseudoUnique
**
**	checkPseudoUnique function checks if the pseudo is unique (for signup purposes)
**	@since 0.0.0
**	@param	String	$pseudo	the pseudo to check
*/
function	checkPseudoUnique($pseudo) {
	$loginManager = new LoginManager();
	$ret = $loginManager->checkPseudoUnique($pseudo, $_SESSION['connected']);
	if (strlen($ret['user_hash']) > 0)
		echo 0;
	else
		echo 1;
}

/*
**	logCheck
**
**	logCheck function log again the user
**	@since 0.0.0
**	@param	String	the password the user entered
*/
function	logCheck($pass) {
	$loginManager = new LoginManager();
	$ret = $loginManager->logCheck($_SESSION['connected'], $pass);
	echo $ret;
}

/*
**	isConnected
**
**	isConnected function checks if the user is connected and if the user hash if valid
**	@since 0.0.0
**	@return	Boolean	true if the user is connected, false otherwise
*/
function	isConnected() {
	$connected = $_SESSION['connected'];
	if (isset($connected)) {
		return (checkUserHash($connected));
	}
	else
		return (0);
}

/*
**	checkUserHash
**
**	checkUserHash function checks if the user hash has a reference in the database
**	@since 0.0.0
**	@param	String	$userHash	the user hash to check
**	@return	Boolean				returns true if the user hash is valid, false otherwise
*/
function	checkUserHash($userHash) {
	$loginManager = new LoginManager();
	$logged = $loginManager->checkUserHash($userHash);
	if (!$logged)
		return (0);
	else {
		return ($logged);
	}
}

/*
**	checkLoginInput
**
**	checkLoginInput function checks inputs for login
**	@since 0.0.0
**	@param	String	$pseudo		new user pseudo
**	@param	String	$password	new user password
*/
function	checkLoginInput($pseudo, $password) {
	$arr = array();
	if (!isset($pseudo))
		array_push($arr, -1);
	elseif (isset($pseudo) && strlen($pseudo) < 4)
		array_push($arr, -1);
	if (!isset($password))
		array_push($arr, -2);
	elseif (isset($password) && !checkPassStrength($password))
		array_push($arr, -2);
	return ($arr);
}

/*
**	checkSignupInput
**
**	checkSignupInput function checks if all inputs are corrects for signup user
**	@since 0.0.0
**	@param	String	$pseudo		new user pseudo
**	@param	String	$email		new user email
**	@param	String	$password	new user password
**	@param	String	$confirm	confirmation of new password
**	@return	Int					minus value if problem (-1: pseudo, -2: email, -3: password len, -4: confirm len, -5: password and confirm missmath, -6: pseudo len)
*/
function	checkSignupInput($pseudo, $email, $password, $confirm) {
	$arr = array();

	if (!isset($pseudo) || strlen($pseudo) < 4)
		array_push($arr, -1);
	if (!isset($email))
		array_push($arr, -2);
	if (!isset($password) || strlen($password) < 6)
		array_push($arr, -3);
	if (!isset($confirm) || strlen($confirm) < 6)
		array_push($arr, -4);
	if (isset($password) && isset($confirm) && $password !== $confirm)
		array_push($arr, -5);
	if (!isset($pseudo) || strlen($pseudo) > 20)
		array_push($arr, -6);
	if (isset($password) && !checkPassStrength($password))
		array_push($arr, -7);
	return ($arr);
}


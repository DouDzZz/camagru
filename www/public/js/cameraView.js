let	streaming		= false,
	video			= document.querySelector("#videoElement"),
	cover			= document.querySelector("#cover"),
	startButton		= document.querySelector("#startButton"),
	queryBrowse		= document.querySelectorAll("#browsePicture");
	browseLabel		= document.getElementById("browseLabel");
	visuPicture		= document.getElementById("visuPicture"),
	shareButton		= document.getElementById("validateButton"),
	cancelButton	= document.getElementById("cancelButton"),
	selectedImgs	= document.getElementsByClassName("imgSelected"),
	img				= document.getElementsByClassName("filterThumbnail"),
	titlePhoto		= document.getElementById("pictureTitle"),
	width			= window.innerWidth * 0.40,
	height			= 0;

startWebcam();
videoCanPlay();
attributeDeleteFunction();


window.onload = function() {
	var x = document.getElementById("browsePicture");
	x.addEventListener("change", loadImage, false);
}

Array.prototype.forEach.call(queryBrowse, function(browse) {
	browse.addEventListener('change', function(e) {
		$.ajax({
			method: 'post',
			url: 'controller/ajaxController.php',
			headers: {
				'Content-type': 'application/x-www-form-urlencoded'
			},
			data: {
				'ajax': true,
				'browsePicture': true
			},
			success: pictureSent,
			error: function(err) {
				// console.log(err)
			}
		});
	});
});

cancelButton.addEventListener('click', hideVisu)

if (selectedImgs.length <= 0) {
	document.getElementById("startButton").style.cursor = "not-allowed";
	startButton.setAttribute("class", "noClickButton");
	browseLabel.setAttribute("class", "noClickButton");
	startButton.setAttribute("title", "Please select a filter picture before");
	browseLabel.setAttribute("title", "Please select a filter picture before");
	document.getElementById("browsePicture").disabled = true;
	startButton.addEventListener("click", pleaseSelectPicture, false);
	browseLabel.addEventListener("click", pleaseSelectPicture, false);
}

titlePhoto.addEventListener('keyup', (ev) => {
	if (ev.keyCode === 13) {
		ev.preventDefault();
		shareCanvas();
	}
});
titlePhoto.addEventListener('input', function(ev) {
	if (this.value.length > 3 && this.value.length < 50) {
		ev.preventDefault();
		shareButton.addEventListener('click', shareCanvas);
		shareButton.classList.remove("noClickButton");
		shareButton.classList.add("normalButton");
		shareButton.style.cursor = "pointer";
	}
	else {
		shareButton.removeEventListener('click', shareCanvas);
		shareButton.classList.add("noClickButton");
		shareButton.classList.remove("normalButton");
		shareButton.style.cursor = "not-allowed";
	}
});
shareButton.classList.add("noClickButton");
shareButton.style.cursor = "not-allowed";

for (i = 0; i < img.length; i++) {
	img.item(i).addEventListener("click", imgClicked);
}

var inputs = document.querySelectorAll('.inputfile');
var changed = false;
var oldValue = document.getElementById("profilePictureId").src;
var darkSwitch = document.getElementById("darkModeSwitcher");
var mailSwitch = document.getElementById("mailSwitcher");
var saveProfile = document.getElementById("saveProfile");
var continueButton = document.getElementById("continueButton");
var backButton = document.getElementById("backButton");


window.onload = function() {
	var x = document.getElementById("file");
	x.addEventListener("change", loadImage, false);
}

document.getElementById("oldPassword").addEventListener("keyup", function(key) {
	if (key.keyCode === 13) {
		postProfile();
	}
});

let timer = 0;
document.getElementById("changePseudo").addEventListener("keyup", function() {
	clearTimeout(timer);
	timer = setTimeout(() => {
		$.ajax({
			method: 'post',
			url: 'controller/ajaxController.php',
			data: {
				'ajax': true,
				'checkPseudoUnique': document.getElementById("changePseudo").value
			},
			success: (data) => {
				if (parseInt(data) === 1) {
					document.getElementById("changePseudo").classList.remove("errorInput");
				}
				else {
					document.getElementById("changePseudo").classList.add("errorInput");
				}
			},
			error: errorAjax
		});
	}, 500);
});

saveProfile.addEventListener('click', function() {
	document.getElementsByClassName("confirmPassword")[0].style.display = "flex";
});

backButton.addEventListener("click", function() {
	document.getElementsByClassName("confirmPassword")[0].style.display = 'none';
})

continueButton.addEventListener('click', postProfile);

cancelButton.addEventListener('click', function() {
	window.location.reload();
});

function	profilePosted(data) {
	window.location.reload();
}

function	relogSuccess(data) {
	if (data === "1") {
		let newPseudo = document.getElementById("changePseudo").value;
		let newMail = document.getElementById("changeEmail").value;
		let newPassword = document.getElementById("changePassword").value;
		let newConfirm = document.getElementById("changeConfirm").value;
		if ((newPassword === "" || newConfirm === "") && newPassword !== newConfirm) {
			window.location.reload();
		}
		else {
			$.ajax({
				method: 'post',
				url: 'controller/ajaxController.php',
				data: {
					'ajax': true,
					'postNewProfile': true,
					'newPseudo': newPseudo,
					'newMail': newMail,
					'newPassword': newPassword,
					'newConfirm': newConfirm
				},
				success: profilePosted,
				error: errorAjax
			});
		}
	}
	// else {
	// 	console.error("Log failed");
	// }
}

function	postProfile() {
	var pass = document.getElementById("oldPassword").value;
	if (pass !== "") {
		$.ajax({
			method: 'post',
			url: 'controller/ajaxController.php',
			data: {
				'ajax': true,
				'logConfirm': pass
			},
			success: relogSuccess,
		});
	}
	// else {
	// 	console.log("You must enter your password to continue");
	// }
}

function	switchedSuccess(data) {
	window.location.reload();
}

function	errorAjax(err) {
	// console.error("error: ", err);
}

document.getElementById("radioButtonMail").addEventListener("click", function() {
	mailSwitch.checked = !mailSwitch.checked;
	$.ajax({
		method: 'post',
		url: 'controller/ajaxController.php',
		data: {
			'ajax': true,
			'mailSwitch': mailSwitch.checked
		},
		success: switchedSuccess,
		error: errorAjax
	});
});

document.getElementById("radioButton").addEventListener("click", function() {
	darkSwitch.checked = !darkSwitch.checked;
	$.ajax({
		method: 'post',
		url: 'controller/ajaxController.php',
		data: {
			'ajax': true,
			'switchDark': darkSwitch.checked
		},
		success: switchedSuccess,
		error: errorAjax
	});
})

Array.prototype.forEach.call(inputs, function(input)
{
	var label	 = input.nextElementSibling,
		labelVal = label.innerHTML;

	input.addEventListener( 'change', function( e )
	{
		changed = true;
		var cancelButton = document.getElementById("cancelButton");
		document.getElementById("pushButton").style.display = "block";
		cancelButton.style.display = "block";
		$.ajax({
			method: 'post',
			url: 'controller/ajaxController.php',
			headers: {
				'Content-type': 'application/x-www-form-urlencoded'
			},
			data: {
				'ajax': true,
				'update': 'picture',
			},
		});
	});
});

function	imageHandler(e2) {
	var store = document.getElementById("profilePictureId");
	store.setAttribute("src", e2.target.result);
}

function	loadImage(e1) {
	var fileName = e1.target.files[0];
	var fr = new FileReader();
	fr.onload = imageHandler;
	fr.readAsDataURL(fileName);
}


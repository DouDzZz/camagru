let x, i, j, selElement, a, b, c;

x = document.getElementsByClassName("selectDiv");
for (i = 0; i < x.length; i++) {
	selElement = x[i].getElementsByTagName("select")[0];
	a = document.createElement("div");
	a.setAttribute("class", "selectSelected");
	a.innerHTML = selElement.options[selElement.selectedIndex].innerHTML;
	x[i].appendChild(a);
	b = document.createElement("div");
	b.setAttribute("class", "selectItems selectHide");
	for (j = 0; j < selElement.length; j++) {
		c = document.createElement("div");
		c.innerHTML = selElement.options[j].innerHTML;
		c.addEventListener("click", function(e) {
			let y, i, k, s, h;
			s = this.parentNode.parentNode.getElementsByTagName("select")[0];
			h = this.parentNode.previousSibling;
			if (this.innerHTML !== "All") {
				var all = document.getElementsByClassName("filterThumbnail");
				var nbItems = -1;
				while (++nbItems < all.length) {
					all.item(nbItems).classList.add("hidePicture");
				}
				var sel = document.getElementsByClassName(this.innerHTML.toLowerCase());
				nbItems = -1;
				while (++nbItems < sel.length) {
					sel.item(nbItems).classList.remove("hidePicture");
				}
			}
			else {
				var all = document.getElementsByClassName("filterThumbnail");
				var nbItems = -1;
				while (++nbItems < all.length) {
					all.item(nbItems).classList.remove("hidePicture")
				}
			}
			for (i = 0; i < s.length; i++) {
				if (s.options[i].innerHTML == this.innerHTML) {
					s.selectedIndex = i;
					h.innerHTML = this.innerHTML;
					y = this.parentNode.getElementsByClassName("sameAsSelected");
					for (k = 0; k < y.length; k++) {
						y[k].removeAttribute("class");
					}
					this.setAttribute("class", "sameAsSelected");
					break;
				}
			}
			h.click();
		});
		b.appendChild(c);
	}
	x[i].appendChild(b);
	a.addEventListener("click", function(e) {
		e.stopPropagation();
		closeAllSelect(this);
		this.nextSibling.classList.toggle("selectHide");
		this.classList.toggle("selectArrowActive");
	});
}

function closeAllSelect(element) {
	let x, y, i, arrNo = [];
	x = document.getElementsByClassName('selectItems');
	y = document.getElementsByClassName("selectSelected");
	for (i = 0; i < y.length; i++) {
		if (element == y[i]) {
			arrNo.push(i);
		} else {
			y[i].classList.remove("selectArrowActive");
		}
	}
	for (i = 0; i < x.length; i++) {
		if (arrNo.indexOf(i)) {
			x[i].classList.add("selectHide");
		}
	}
}
document.addEventListener("click", closeAllSelect);
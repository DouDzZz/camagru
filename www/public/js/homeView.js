var offset = 0;
var limit = 24;
var nbPictures = 0;
var connected = false;

Promise.all([isConnected()]).then((data) => {
	connected = data && data[0] && JSON.parse(data[0]) ? true : false;
});

getPicture();
getButtons();

function	isConnected() {
	return ajaxIsConnected();
}

async function	checkConnection() {
	connected = await isConnected();
}

function	backFunction() {
	var pictureGrid = document.getElementsByClassName("pictureGrid");
	var buttonDiv = document.getElementsByClassName("buttonDiv");
	var sum = document.getElementsByClassName("summary");
	let pictureCard = document.getElementById("pictureCard");

	pictureCard.childNodes.forEach(elem => {
		console.log(elem);
		elem.remove();
	});

	if (offset - limit >= 0)
		offset -= limit;

	pictureGrid[0].remove();
	buttonDiv[0].remove();
	sum[0].remove();

	getPicture();
	getButtons();
}

function	nextFunction() {
	var pictureGrid = document.getElementsByClassName("pictureGrid");
	var buttonDiv = document.getElementsByClassName("buttonDiv");
	var sum = document.getElementsByClassName("summary");
	let pictureCard = document.getElementById("pictureCard");

	pictureCard.childNodes.forEach(elem => {
		console.log(elem);
		elem.remove();
	});

	if (offset + limit < nbPictures)
		offset += limit;

	pictureGrid[0].remove();
	buttonDiv[0].remove();
	sum[0].remove();

	getPicture();
	getButtons();
}

function	sqlDateToJs(sqlDate, options = {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}) {
	var sqlDateArr1		= sqlDate.split("-");
	var sYear			= sqlDateArr1[0];
	var sMonth			= (Number(sqlDateArr1[1]) - 1).toString();
	var sqlDateArr2		= sqlDateArr1[2].split(" ");
	var sDay			= sqlDateArr2[0];
	var sqlDateArr3		= sqlDateArr2[1].split(":");
	var sHour			= sqlDateArr3[0];
	var sMinute			= sqlDateArr3[1];
	var sqlDateArr4		= sqlDateArr3[2].split(".");
	var sSecond			= sqlDateArr4[0];
	var sMillisecond	= sqlDateArr4[1] != undefined ? sqlDateArr4[1] : 0;
	var lang			= window.navigator.language;
	var newDate			= new Date(sYear,sMonth,sDay,sHour,sMinute,sSecond,sMillisecond);
	var finalStr		= newDate.toLocaleDateString(lang, options);

	return finalStr;
}

function	escapeHtml(str) {
    return str
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
}

function	createCommentsList(data) {
	if (document.getElementById("ulId"))
		document.getElementById("ulId").remove();
	const card = document.getElementsByClassName("card");
	if (document.getElementsByClassName("commentDiv").length > 0)
		document.getElementsByClassName("commentDiv")[0].remove();
	const commentDiv = document.createElement("div");
	const ul = document.createElement("ul");

	ul.setAttribute("id", "ulId");
	if (data && data.length != undefined) {
		let i = 0;
		jsonData = JSON.parse(data);
		if (jsonData.length > 0) {
			jsonData.forEach(comment => {
				const li = document.createElement("li");
				const profile = document.createElement("img");
				const pseudoSpan = document.createElement("span");
				const span = document.createElement("span");
				const profileDiv = document.createElement("div");
				const timestamp = document.createElement("span");
				const commentCore = document.createElement("div");

				profile.setAttribute("class", "profilePicture");
				profileDiv.setAttribute("class", "profileDiv");
				timestamp.setAttribute("class", "timestamp");
				commentCore.setAttribute("class", "commentCore");
				if (comment.profile_picture == null)
					profile.setAttribute("src", "public/imgs/tools/default_profile.jpg");
				else
					profile.setAttribute("src", comment.profile_picture);
				span.innerHTML = escapeHtml(comment.comment);
				pseudoSpan.innerHTML = escapeHtml(comment.pseudo)+"<br>";
				timestamp.innerHTML = sqlDateToJs(comment.timestamp, {
					year: '2-digit',
					month: '2-digit',
					day: '2-digit',
					hour: '2-digit',
					minute: '2-digit',
					second: '2-digit',hourCycle: 'h24'
				});
				span.setAttribute("class", "commentSpan");
				pseudoSpan.setAttribute("class", "pseudoSpan");
				profileDiv.appendChild(profile);
				profileDiv.appendChild(pseudoSpan);
				profileDiv.appendChild(timestamp);
				commentCore.appendChild(span);
				li.appendChild(profileDiv);
				li.appendChild(commentCore);
				ul.appendChild(li);
			});
		}
	}
	commentDiv.appendChild(ul);
	commentDiv.setAttribute("class", "commentDiv");
	appendIfConnected(card[0], commentDiv);
	// card[0].appendChild(commentDiv);
}

function	createComments(pictureId) {
	const card = document.getElementsByClassName("card");
	if (document.getElementsByClassName("commentInput").length > 0)
		document.getElementsByClassName("commentInput")[0].remove();
	const commentInput = document.createElement("textarea");

	commentInput.addEventListener('focus', () => commentInput.style.height = "95%");
	commentInput.addEventListener('blur', () => commentInput.style.height = "30px");
	commentInput.addEventListener("keyup", (ev) => {
		if (ev.keyCode === 13) {
			ev.preventDefault();
			sendCommentDb(pictureId);
		}
	})
	commentInput.setAttribute("maxlength", "64000");
	commentInput.setAttribute("class", "commentInput");
	commentInput.setAttribute("placeholder", "Write your comment");
	appendIfConnected(card[0], commentInput);
	ajaxGetComments(pictureId, createCommentsList);
}

function	commentSent(pictureId) {
	removeIfClassExists("commentInput");
	removeIfClassExists("commentDiv");
	createComments(pictureId);
	ajaxSendComment(pictureId, )
}

function	sendCommentDb(pictureId) {
	var comment = document.getElementsByClassName("commentInput")[0].value;
	var callback = (data) => {
		console.log(data);
		commentSent(pictureId);
		ajaxSendNotificationMail(pictureId);
	}
	if (comment.length > 0) {
		ajaxSendComment(pictureId, comment, callback);
		commentSent(pictureId)
	}
}

function	removeIfClassExists(className) {
	if (document.getElementsByClassName(className).length > 0)
		document.getElementsByClassName(className)[0].remove();
}

function	pictureClicked() {
	const parent = this.parentNode;
	const pictureSrc = parent.firstChild.src;
	const pictureId = parent.firstChild.id;
	const pictureDiv = document.getElementById("pictureDetail");
	const pictureCard = document.getElementById("pictureCard");

	removeIfClassExists("dataDiv");
	removeIfClassExists("sendComment");
	removeIfClassExists("backButton");
	if (document.getElementById("likeDiv"))
		document.getElementById("likeDiv").remove();
	if (parent && pictureSrc) {
		const	img = document.createElement("img");
		const	h2 = document.createElement("h2");
		const	postedBy = document.createElement("span");
		const	postedOn = document.createElement("span");
		const	dataAndLike = document.createElement("div");
		const	dataDiv = document.createElement("div");
		const	likeData = document.createElement("span");
		const	likeDiv = document.createElement("div");
		const	likeButton = document.createElement("button");
		const	likeImg = document.createElement("img");
		const	sendComment = document.createElement("button");
		const	back = document.createElement("button");

		dataDiv.setAttribute("class", "dataDiv");
		postedBy.innerHTML = this.childNodes[1].childNodes[0].innerHTML+"<br>";
		postedOn.innerHTML = this.childNodes[1].childNodes[1].innerHTML+"<br>";
		img.setAttribute('src', pictureSrc);
		likeImg.setAttribute('id', "likeImg");
		userLikedIt(pictureId);
		likeImg.classList.add('likeImg');
		likeButton.appendChild(likeImg);
		likeDiv.appendChild(likeButton);
		likeDiv.classList.add("likeDiv");
		h2.innerHTML = this.firstChild.innerHTML;
		pictureCard.appendChild(img);
		dataDiv.appendChild(h2);
		dataDiv.appendChild(postedBy);
		dataDiv.appendChild(postedOn);
		sendComment.innerHTML = "Send comment";
		sendComment.setAttribute("class", "sendComment");
		back.setAttribute("class", "backButton");
		back.innerHTML = "Back";
		likeButton.addEventListener('click', () => likePicture(pictureId));
		sendComment.addEventListener('click', () => sendCommentDb(pictureId));
		back.addEventListener('click', () => {
			while (pictureCard.firstChild) {
				pictureCard.removeChild(pictureCard.firstChild);
			}
			document.getElementById("pictureDetail").style.display = "none"
		})
		dataAndLike.classList.add('dataAndLike');
		likeData.setAttribute('id', "nbOfLikes");
		getLikes(pictureId);
		dataDiv.appendChild(likeData);
		dataAndLike.appendChild(dataDiv);
		appendIfConnected(pictureCard, likeDiv);
		pictureCard.appendChild(dataAndLike);
		appendIfConnected(pictureDiv, sendComment);
		pictureCard.appendChild(back);
		pictureDiv.style.display = "flex";
		createComments(pictureId);
	}
}

function	userLikedIt(pictureId) {
	var callback = (data) => {
		var imgPath = data == 1
			? 'public/imgs/tools/like_filled.png'
			: 'public/imgs/tools/like.png';
		let likeImgElem = document.getElementById("likeImg")
		if (likeImgElem)
			likeImgElem.setAttribute('src', imgPath);
	}
	ajaxUserLikedIt(pictureId, callback);
}

function	getLikes(pictureId) {
	var callback = (data) => document.getElementById("nbOfLikes").innerHTML = "Likes: " + data;
	ajaxGetLikes(pictureId, callback);
}

function	likePicture(pictureId) {
	var callback = (data) => {
		if (!data)
			alert("You must be log to like a picture");
		userLikedIt(pictureId);
		getLikes(pictureId);
		ajaxSendNotificationMail(pictureId);
	}
	ajaxLikePictures(pictureId, callback);
}

function	appendIfConnected(element, toAppend) {
	connected ? element.appendChild(toAppend) : 0;
}

function	createPicture(picture, pictureGrid) {
	let date = sqlDateToJs(picture.timestamp, {year: 'numeric', month: 'long', day: '2-digit'});
	let photo = document.createElement("div");
	let homePicture = document.createElement("img");
	let hoverPhoto = document.createElement("div");
	let h3 = document.createElement("h3");
	let userData = document.createElement("div");
	let userPseudo = document.createElement("span");
	let creationDate = document.createElement("span");
	let nbOfComments = document.createElement("span");
	let nbOfLikes = document.createElement("span");

	photo.setAttribute('class', 'photo');
	homePicture.setAttribute('class', 'homePicture');
	homePicture.setAttribute('src', picture.image_url);
	homePicture.setAttribute('title', picture.title);
	homePicture.setAttribute("id", picture.id);
	hoverPhoto.setAttribute('class', "hoverPhoto");
	hoverPhoto.addEventListener('click', pictureClicked);
	userData.setAttribute("class", "userData");
	h3.innerHTML = picture.title;
	photo.appendChild(homePicture);
	photo.appendChild(hoverPhoto);
	hoverPhoto.appendChild(h3);
	userPseudo.innerHTML = "posted by: " + picture.pseudo;
	creationDate.innerHTML = date;
	if (picture.nb_of_comments > 1)
		nbOfComments.innerHTML = picture.nb_of_comments+" comments";
	else if (picture.nb_of_comments > 0)
		nbOfComments.innerHTML = picture.nb_of_comments+" comment";
	else
		nbOfComments.innerHTML = "No comment";
	if (picture.nb_of_likes > 1)
		nbOfLikes.innerHTML = picture.nb_of_likes+" people like this";
	else if (picture.nb_of_likes > 0)
		nbOfLikes.innerHTML = picture.nb_of_likes+" person like this";
	else
		nbOfLikes.innerHTML = "no like";
	userData.appendChild(userPseudo);
	userData.appendChild(creationDate);
	userData.appendChild(nbOfComments);
	userData.appendChild(nbOfLikes);
	hoverPhoto.appendChild(userData);
	pictureGrid.appendChild(photo);
}

function	createButton(div, back) {
	let button = document.createElement("button");

	button.setAttribute("class", "buttonNav");
	button.setAttribute("type", "button");
	button.addEventListener('click', back == 1 ? backFunction : nextFunction);
	button.innerHTML = back == 1 ? "Prev" : "Next";
	div.appendChild(button);
}

function	createNavButtons(nb) {
	nbPictures = nb;
	let body = document.getElementById("homeBody");
	let div = document.createElement("div");
	let sum = document.createElement("span");

	sum.innerHTML = Math.round(offset / limit) + 1 + " / " + Math.ceil(nbPictures / limit);
	sum.setAttribute("class", "summary");
	if (offset > 0 || nbPictures > offset + limit)
		body.appendChild(sum);
	if (offset > 0)
		createButton(div, 1);
	if (nbPictures > offset + limit)
		createButton(div, 0);
	div.setAttribute("class", "buttonDiv");
	body.appendChild(div);
}

function	showNoPicture(jsonData) {
	noPicture = document.getElementById("noPicture");
	if (jsonData.length === undefined)
		noPicture.style.display = "flex";
	else
		noPicture.style.display = "none";
	return (jsonData.length === undefined);
}

function	createPictureGrid(data) {
	if (!data) {
		noPicture.style.display = "flex";
		return ;
	}
	else
		noPicture.style.display = "flex";
	let jsonData = JSON.parse(data);
	if (showNoPicture(jsonData))
		return ;
	pictureGrid = document.createElement("div");
	pictureGrid.setAttribute("class", "pictureGrid");
	document.getElementById("home").appendChild(pictureGrid);
	jsonData.forEach(picture => {
		createPicture(picture, pictureGrid);
	});
}

function	getButtons() {
	ajaxGetButtons(createNavButtons);
}

function getPicture() {
	ajaxGetPicture(offset, limit, createPictureGrid);
}

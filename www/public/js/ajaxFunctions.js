function	ajaxDefaultErrorExit(err) {
	console.error(err);
}

function	ajaxCaller(data, success, error, method = 'post', url = 'controller/ajaxController.php') {
	$.ajax({
		method,
		url,
		data,
		success,
		error
	});
}

function	ajaxIsConnected() {
	data = {
		'ajax': true,
		'isConnected': true
	};
	return $.ajax({
		method: 'post',
		url: 'controller/ajaxController.php',
		data
	});
}

function	ajaxGetComments(pictureId, callback) {
	data = {
		'ajax': true,
		'get_comments': pictureId
	};
	ajaxCaller(data, callback);
}

function	ajaxSendNotificationMail(pictureId) {
	data = {
		'ajax': true,
		'sendNotificationMail': true,
		'pictureId': pictureId
	}
	ajaxCaller(data);
}

function	ajaxSendComment(pictureId, comment, callback) {
	data = {
		'ajax': true,
		'sendComment': comment,
		'pictureId': pictureId
	};
	ajaxCaller(data, callback);
}

function	ajaxUserLikedIt(pictureId, callback) {
	data = {
		'ajax': true,
		'userLikedIt': true,
		'pictureId': pictureId
	};
	ajaxCaller(data, callback);
}

function	ajaxGetLikes(pictureId, callback) {
	data = {
		'ajax': true,
		'getLikes': true,
		'pictureId': pictureId
	};
	ajaxCaller(data, callback);
}

function	ajaxLikePictures(pictureId, callback) {
	data = {
		'ajax': true,
		'likePicture': true,
		'pictureId': pictureId
	};
	ajaxCaller(data, callback);
}

function	ajaxGetButtons(callback) {
	data = {
		'ajax': true,
		'get_nb_of_pictures': true
	};
	ajaxCaller(data, callback);
}

function	ajaxGetPicture(offset, limit, callback) {
	data = {
		'ajax': true,
		'getHomePictures': true,
		'offset': offset,
		'limmit': limit
	};
	ajaxCaller(data, callback);
}

var pushing = false;
var noWebcam = false;

function	startWebcam() {
	if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		navigator.mediaDevices.getUserMedia({video: true, audio: false})
		.then(function(stream) {
			video.srcObject = stream;
			video.play();
		})
		.catch(function(error) {
			noWebcam = true;
			// console.error("Something went wrong, maybe you denied camera access rights.\nGenerated error:", error);
		})
	}
}

function	pictureDeleted(data) {
	pushing = false;
	window.location.reload();
}

function	errorAjax(err) {
	pushing = false;
}

function	deletePicture(ev) {
	if (pushing === true)
		return ;
	pushing = true;
	if (this.childNodes.length > 1 && this.childNodes[1].tagName === "IMG") {
		$.ajax({
			method: 'post',
			url: 'controller/ajaxController.php',
			data: {
				'ajax': true,
				'deletePicture': this.childNodes[1].id
			},
			success: pictureDeleted,
			error: errorAjax
		});
	}
}

function	attributeDeleteFunction() {
	var userPictures = document.getElementsByClassName("userPicture");
	for (var i = 0; i < userPictures.length; i++) {
		userPictures[i].addEventListener('click', deletePicture);
	}
}

function	videoCanPlay() {
	video.addEventListener('canplay', function(ev) {
		if (!streaming) {
			height = video.videoHeight / (video.videoWidth / width);
			video.setAttribute('width', width);
			video.setAttribute('height', height);
			streaming = true;
		}
	}, false);
}

function	pleaseSelectPicture() {
	alert("Please select a filter");
}

function	takePicture() {
	let store = document.getElementById("canvasDiv");
	if (document.getElementById("canvas"))
		document.getElementById("canvas").remove();
	let newImg = document.createElement("canvas");
	newImg.setAttribute("name", "canvasPicture");
	newImg.setAttribute("id", "canvas");
	store.appendChild(newImg);
	newImg.width = width;
	newImg.height = height;
	newImg.getContext('2d').drawImage(video, 0, 0, width, height);
	const data = newImg.toDataURL('image/png');
}

function	showVisu() {
	visuPicture.style.visibility = "visible";
}

function	hideVisu() {
	window.location.reload();
	// visuPicture.style.visibility = "hidden";
}

function	uploadSuccess(data) {
	pushing = false;
	window.location.reload();
}

function	uploadFailure(err) {
	pushing = false;
	// console.error(err);
}

function	shareCanvas(ev) {
	if (pushing)
		return ;
	pushing = true;
	let imgs = document.getElementsByClassName("imgSelected");
	let pictureTitle = document.getElementById("pictureTitle").value;
	var filterAddress, dataURL, width, height;
	var i = new Image();

	if (imgs.length > 0)
		filterAddress = imgs[0].getAttribute('src');
	if (document.getElementById("canvas").tagName === "CANVAS")
		dataURL = canvas.toDataURL('image/png');
	else if (document.getElementById("canvas").tagName === "IMG")
		dataURL = document.getElementById("canvas").src;
	i.onload = function() {
		width = i.width;
		height = i.height;
	};
	i.src = dataURL;
	$.ajax({
		method: 'post',
		url: 'controller/ajaxController.php',
		data: {
			'ajax': true,
			'hidden_input': dataURL,
			'filter_address': filterAddress,
			'picture_title': pictureTitle,
			'width': width,
			'height': height
		},
		success: uploadSuccess,
		error: uploadFailure
	});
}

function	startButtonEvent(ev) {
	takePicture();
	showVisu();
	ev.preventDefault();
}

function	pictureSent(data) {
	// console.log(data);
}

function	imageHandler(e2) {
	var store = document.getElementById("canvasDiv");
	var newImg = document.createElement("img");
	if (document.getElementById("canvas"))
		document.getElementById("canvas").remove();
	newImg.setAttribute("src", e2.target.result);
	newImg.style.objectFit = "cover";
	newImg.setAttribute("id", "canvas");
	store.appendChild(newImg);
	showVisu();
}

function	loadImage(e1) {
	var fileName = e1.target.files[0];
	var fr = new FileReader();
	if (fileName) {
		if (fileName.size < 4000000
			&& (
				fileName.type === "image/jpg"
				|| fileName.type === "image/jpeg"
				|| fileName.type === "image/png"
				|| fileName.type === "image/gif"
			)) {
			fr.onload = imageHandler;
			fr.readAsDataURL(fileName);
		}
		else if (
			fileName.type !== "image/jpg"
			&& fileName.type !== "image/jpeg"
			&& fileName.type !== "image/png"
			&& fileName.type !== "image/gif"
		)
			alert("Not supported type");
		else {
			alert("image too big");
		}
	}
}

function	imgClicked() {
	let imgs = document.getElementsByClassName("imgSelected");
	var same = false;
	for (i = 0; i < this.classList.length; i++) {
		if (this.classList[i] == "imgSelected")
			same = true;
	}
	for (i = 0; i < imgs.length; i++) {
		imgs.item(i).classList.remove("imgSelected");
	}
	if (!same) {
		this.classList.add("imgSelected");
		if (!document.getElementById("filterPicture")) {
			var newImg = document.createElement("img");
			newImg.setAttribute("id", "filterPicture");
			var newImg2 = document.createElement("img");
			newImg2.setAttribute("id", "filterPicture2");
			document.getElementById("innerMain").appendChild(newImg);
			document.getElementById("canvasDiv").appendChild(newImg2)
		}
		document.getElementById("filterPicture").setAttribute("src", this.getAttribute("src"));
		document.getElementById("filterPicture2").setAttribute("src", this.getAttribute("src"));
		document.getElementById("filterPicture").style.opacity = 1
		document.getElementById("filterPicture2").style.opacity = 1;
		var anchor = document.getElementById("videoElement");
		anchor.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});

		if (!noWebcam) {
			document.getElementById("startButton").style.cursor = "pointer";
			startButton.style.cursor = "not-allowed";
			startButton.setAttribute("class", "normalButton");
			startButton.removeEventListener('click', pleaseSelectPicture, false);
			startButton.addEventListener('click', startButtonEvent, false);
		}

		document.getElementById("browsePicture").style.cursor = "pointer";
		browsePicture.style.cursor = "not-allowed";
		browsePicture.disabled = false;
		document.getElementById("browseLabel").setAttribute("class", "normalButton");
		document.getElementById('browseLabel').removeEventListener('click', pleaseSelectPicture, false);
		browsePicture.addEventListener('click', browsePicture, false);
	}
	else {
		document.getElementById("filterPicture").removeAttribute("src");
		document.getElementById("filterPicture2").removeAttribute("src");
		document.getElementById("filterPicture").style.opacity = 0;
		document.getElementById("filterPicture2").style.opacity = 0;
		if (!noWebcam) {
			document.getElementById("startButton").style.cursor = "not-allowed";
			startButton.setAttribute("class", "noClickButton");
			startButton.removeEventListener('click', startButtonEvent, false);
			startButton.addEventListener('click', pleaseSelectPicture, false);
		}

		browsePicture.disabled = true;
		document.getElementById("browsePicture").style.cursor = "not-allowed";
		document.getElementById("browseLabel").setAttribute("class", "noClickButton");
		document.getElementById("browseLabel").addEventListener('click', pleaseSelectPicture, false);
	}
}

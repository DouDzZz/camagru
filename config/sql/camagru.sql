-- put in ./dump directory

DROP DATABASE IF EXISTS camagru_db;
CREATE DATABASE IF NOT EXISTS camagru_db; USE camagru_db;

CREATE TABLE `Users` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`pseudo` VARCHAR(20) NOT NULL UNIQUE,
	`user_hash` VARCHAR(128) NOT NULL UNIQUE,
	`profile_picture` VARCHAR(255) UNIQUE,
	`email` VARCHAR(100) NOT NULL UNIQUE,
	`password` VARCHAR(128) NOT NULL,
	`active` INT,
	`activation` VARCHAR(128) NOT NULL UNIQUE
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `Admin` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL UNIQUE
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `Pictures` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`image_url` VARCHAR(255) NOT NULL,
	`title` VARCHAR(50) NOT NULL,
	`timestamp` TIMESTAMP
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `Likes` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`picture_id` INT NOT NULL,
	`user_id` INT NOT NULL,
	`timestamp` TIMESTAMP
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `MailPreference` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`preference` INT NOT NULL
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `Comments` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`picture_id` INT NOT NULL,
	`comment` BLOB NOT NULL,
	`timestamp` TIMESTAMP
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `Dark` (
	`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`dark` INT NOT NULL
) ENGINE=InnoDB CHARSET=utf8;

INSERT INTO `Users` (`pseudo`, `email`, `password`, `user_hash`, `active`, `activation`) VALUE
(
	'root',
	'edouard.jubert@gmail.com',
	'84e9a41ced62cd32a0bd2d3021984a4c81813e5ac10c38a398f2deec3043f31beb44ecfd737cf546fd191812197fae8005b001de34ecf7323d9bdfb2fc4646e8',
	'84e9a41ced62cd32a0bd2d3021984a4c81813e5ac10c38a398f2deec3043f31beb44ecfd737cf546fd191812197fae8005b001de34ecf7323d9bdfb2fc4646e8',
	1,
	'84e9a41ced62cd32a0bd2d3021984a4c81813e5ac10c38a398f2deec3043f31beb44ecfd737cf546fd191812197fae8005b001de34ecf7323d9bdfb2fc4646e8'
),
(
	'edjubert',
	'edouard.jubert@hotmail.fr',
	'690490161a6355a37145cede1b969b9db7b035231b2db3eaf5aa87cb2dfe500a90d14c87c97e71b69981a78ebc77b3acb670095bc3984548e526c5e1513009f6',
	'690490161a6355a37145cede1b969b9db7b035231b2db3eaf5aa87cb2dfe500a90d14c87c97e71b69981a78ebc77b3acb670095bc3984548e526c5e1513009f6',
	1,
	'690490161a6355a37145cede1b969b9db7b035231b2db3eaf5aa87cb2dfe500a90d14c87c97e71b69981a78ebc77b3acb670095bc3984548e526c5e1513009f6'
);

INSERT INTO `Admin` (`user_id`) VALUE
(1), (2);

INSERT INTO `MailPreference` (`user_id`, `preference`) VALUE (1, 1), (2, 1);

INSERT INTO `Dark` (`user_id`, `Dark`) VALUE (1, 1), (2, 1);

<?php
require_once("database.php");

try {
	$SQL_FILE_PATH = "sql/camagru.sql";
	$host = new PDO("mysql:host=".$DB_HOST, $DB_USER, $DB_PASS);
	$DB_NAME = $host->quote($DB_NAME);
	if (file_exists($SQL_FILE_PATH)) {
		$sql = file_get_contents($SQL_FILE_PATH);
		$cmd = $host->prepare($sql);
		$ret = $cmd->execute();
		if ($ret)
			echo "All Done!";
		else
			throw new PDOException("Error while uploading sql file", 500);
	}
	else {
		throw new Exception("$SQL_FILE_PATH: No file found");
	}
}
catch (Exception $e) {
	echo "Cannot reach DB(".$e->getCode()."): ".$e->getMessage();
}
